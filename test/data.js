// @ts-check

const DiscordTypes = require("discord-api-types/v10")

module.exports = {
	channel: {
		general: {
			type: 0,
			topic: "https://docs.google.com/document/d/blah/edit | I spread, pipe, and whip because it is my will. :headstone:",
			rate_limit_per_user: 0,
			position: 0,
			permission_overwrites: [],
			parent_id: null,
			nsfw: false,
			name: "collective-unconscious" ,
			last_pin_timestamp: "2023-04-06T09:51:57+00:00",
			last_message_id: "1103832925784514580",
			id: "112760669178241024",
			default_thread_rate_limit_per_user: 0,
			guild_id: "112760669178241024"
		},
		updates: {
			type: 0,
			topic: "Updates and release announcements for Out Of Your Element.",
			rate_limit_per_user: 0,
			position: 0,
			permission_overwrites: [{
				type: 0,
				id: "112760669178241024",
				deny: "2048",
				allow: "0"
			}],
			parent_id: null,
			nsfw: false,
			name: "updates",
			last_message_id: "1329413270196715564",
			id: "1161864271370666075",
			guild_id: "112760669178241024"
		},
		saving_the_world: {
			type: 0,
			topic: "Anything and everything archiving/preservation related",
			rate_limit_per_user: 0,
			position: 0,
			permission_overwrites: [],
			parent_id: null,
			name: "saving-the-world",
			last_pin_timestamp: "2021-04-14T18:39:41+00:00",
			last_message_id: "1335828749479837750",
			id: "665310973967597573",
			flags: 0,
			guild_id: "665289423482519565"
		}
	},
	room: {
		general: {
			"m.room.name/": {name: "main"},
			"m.room.topic/": {topic: "#collective-unconscious | https://docs.google.com/document/d/blah/edit | I spread, pipe, and whip because it is my will. :headstone:\n\nChannel ID: 112760669178241024\nGuild ID: 112760669178241024"},
			"m.room.guest_access/": {guest_access: "can_join"},
			"m.room.history_visibility/": {history_visibility: "shared"},
			"m.space.parent/!jjmvBegULiLucuWEHU:cadence.moe": {
				via: ["cadence.moe"],
				canonical: true
			},
			"m.room.join_rules/": {
				join_rule: "restricted",
				allow: [{
					type: "m.room_membership",
					room_id: "!jjmvBegULiLucuWEHU:cadence.moe"
				}]
			},
			"m.room.avatar/": {
				url: {$url: "/icons/112760669178241024/a_f83622e09ead74f0c5c527fe241f8f8c.png?size=1024"}
			},
			"m.room.power_levels/": {
				events_default: 0,
				events: {
					"m.reaction": 0
				},
				users: {
					"@test_auto_invite:example.org": 100
				},
				notifications: {
					room: 0
				}
			},
			"chat.schildi.hide_ui/read_receipts": {},
			"uk.half-shot.bridge/moe.cadence.ooye://discord/112760669178241024/112760669178241024": {
				bridgebot: "@_ooye_bot:cadence.moe",
				protocol: {
					id: "discord",
					displayname: "Discord"
				},
				network: {
					id: "112760669178241024",
					displayname: "Psychonauts 3",
					avatar_url: {$url: "/icons/112760669178241024/a_f83622e09ead74f0c5c527fe241f8f8c.png?size=1024"}
				},
				channel: {
					id: "112760669178241024",
					displayname: "collective-unconscious",
					external_url: "https://discord.com/channels/112760669178241024/112760669178241024"
				}
			}
		}
	},
	guild: {
		/** @type {DiscordTypes.APIGuild} */ // @ts-ignore
		general: {
			owner_id: "112760500130975744",
			premium_tier: 3,
			stickers: [{
				type: 2,
				tags: "sunglasses",
				name: "pomu puff",
				id: "1106323941183717586",
				guild_id: "112760669178241024",
				format_type: 1,
				description: "damn that tiny lil bitch really chuffing. puffing that fat ass dart",
				available: true
			}],
			max_members: 500000,
			splash: "86a34ed02524b972918bef810087f8e7",
			explicit_content_filter: 0,
			afk_channel_id: null,
			nsfw_level: 0,
			description: null,
			preferred_locale: "en-US",
			system_channel_id: "112760669178241024",
			mfa_level: 0,
			/** @type {300} */
			afk_timeout: 300,
			id: "112760669178241024",
			icon: "a_f83622e09ead74f0c5c527fe241f8f8c",
			emojis: [
				{
					roles: [],
					require_colons: true,
					name: "hippo",
					managed: false,
					id: "230201364309868544",
					available: true,
					animated: false
				},
				{
					roles: [],
					require_colons: true,
					name: "hipposcope",
					managed: false,
					id: "393635038903926784",
					available: true,
					animated: true
				}
			],
			premium_subscription_count: 14,
			roles: [
				{
					unicode_emoji: null,
					tags: {},
					position: 0,
					permissions: '559623605575360',
					name: '@everyone',
					mentionable: false,
					managed: false,
					id: '112760669178241024',
					icon: null,
					hoist: false,
					flags: 0,
					color: 0
				},
				{
					unicode_emoji: null,
					tags: {},
					position: 22,
					permissions: '0',
					name: 'Master Wonder Mage',
					mentionable: true,
					managed: false,
					id: '503685967463448616',
					icon: null,
					hoist: false,
					flags: 0,
					color: 0
				}, {
					unicode_emoji: null,
					tags: {},
					position: 131,
					permissions: '0',
					name: '!!DLCS!!',
					mentionable: true,
					managed: false,
					id: '212762309364285440',
					icon: null,
					hoist: true,
					flags: 0,
					color: 11076095
				}, {
					unicode_emoji: '🍂',
					tags: {},
					position: 102,
					permissions: '0',
					name: 'corporate overlord',
					mentionable: false,
					managed: false,
					id: '217013981053845504',
					icon: null,
					hoist: true,
					flags: 0,
					color: 16745267
				}
			],
			discovery_splash: null,
			default_message_notifications: 1,
			region: "deprecated",
			max_video_channel_users: 25,
			verification_level: 0,
			application_id: null,
			premium_progress_bar_enabled: false,
			banner: "a_a666ae551605a2d8cda0afd591c0af3a",
			features: [],
			vanity_url_code: null,
			hub_type: null,
			public_updates_channel_id: null,
			rules_channel_id: null,
			name: "Psychonauts 3",
			max_stage_video_channel_users: 300,
			system_channel_flags: 0|0,
			safety_alerts_channel_id: null
		},
		fna: {
			application_id: null,
			roles: [],
			activity_instances: [],
			banner: null,
			stickers: [],
			joined_at: "2020-04-25T07:36:09.644000+00:00",
			default_message_notifications: 1,
			afk_timeout: 60,
			clan: null,
			hub_type: null,
			afk_channel_id: "216367750216548362",
			discovery_splash: null,
			splash: null,
			explicit_content_filter: 0,
			max_members: 500000,
			premium_subscription_count: 0,
			voice_states: [],
			id: "66192955777486848",
			premium_tier: 0,
			name: "Function & Arg",
			premium_progress_bar_enabled: false,
			icon: "8bfeb3237cd8697d1d1cd5c626ca8cea",
			large: true,
			verification_level: 0,
			public_updates_channel_id: null,
			stage_instances: [],
			rules_channel_id: null,
			emojis: [],
			owner_id: "66186356581208064",
			threads: [],
			max_stage_video_channel_users: 50,
			description: null,
			unavailable: false,
			features: [
				"CHANNEL_ICON_EMOJIS_GENERATED",
				"NEW_THREAD_PERMISSIONS",
				"THREADS_ENABLED",
				"SOUNDBOARD"
			],
			latest_onboarding_question_id: null,
			max_video_channel_users: 25,
			home_header: null,
			mfa_level: 0,
			system_channel_id: null,
			guild_scheduled_events: [],
			nsfw_level: 0,
			vanity_url_code: null,
			member_count: 966,
			presences: [],
			application_command_counts: {},
			system_channel_flags: 0,
			preferred_locale: "en-US",
			region: "deprecated",
			inventory_settings: null,
			soundboard_sounds: [],
			version: 1711491959939,
			incidents_data: null,
			embedded_activities: [],
			nsfw: false,
			safety_alerts_channel_id: null,
			lazy: true
		},
		data_horde: {
			preferred_locale: "en-US",
			afk_channel_id: null,
			profile: null,
			owner_id: "222343226990788609",
			soundboard_sounds: [],
			hub_type: null,
			mfa_level: 0,
			activity_instances: [],
			inventory_settings: null,
			voice_states: [],
			system_channel_id: "675397790204952636",
			id: "665289423482519565",
			member_count: 138,
			clan: null,
			default_message_notifications: 1,
			name: "Data Horde",
			banner: null,
			premium_subscription_count: 0,
			max_stage_video_channel_users: 50,
			max_members: 500000,
			incidents_data: null,
			joined_at: "2020-05-10T02:00:10.646000+00:00",
			unavailable: false,
			discovery_splash: null,
			threads: [],
			system_channel_flags: 0,
			safety_alerts_channel_id: null,
			nsfw: false,
			nsfw_level: 0,
			stage_instances: [],
			large: false,
			icon: "d7c4bdb35c10f21e475a50fb205d5c32",
			roles: [
				{
					version: 1683238686112,
					unicode_emoji: null,
					tags: {},
					position: 0,
					permissions: "2221982107557441",
					name: "@everyone",
					mentionable: false,
					managed: false,
					id: "665289423482519565",
					icon: null,
					hoist: false,
					flags: 0,
					color: 0
				},
				{
					version: 1683791258594,
					unicode_emoji: null,
					tags: {},
					position: 22,
					permissions: "7515668211",
					name: "Founder",
					mentionable: true,
					managed: false,
					id: "665290147377578005",
					icon: null,
					hoist: false,
					flags: 0,
					color: 1752220
				},
				{
					version: 1683791258580,
					unicode_emoji: null,
					tags: {},
					position: 19,
					permissions: "6546775617",
					name: "Gaming Alexandria",
					mentionable: false,
					managed: false,
					id: "684524730274807911",
					icon: null,
					hoist: false,
					flags: 0,
					color: 15844367
				}
			],
			description: null,
			afk_timeout: 300,
			verification_level: 1,
			latest_onboarding_question_id: null,
			guild_scheduled_events: [],
			rules_channel_id: null,
			embedded_activities: [],
			region: "deprecated",
			vanity_url_code: null,
			application_id: null,
			premium_tier: 0,
			explicit_content_filter: 0,
			stickers: [],
			public_updates_channel_id: null,
			splash: null,
			premium_progress_bar_enabled: false,
			features: [],
			lazy: true,
			max_video_channel_users: 25,
			application_command_counts: {},
			home_header: null,
			version: 1717720047590,
			emojis: [],
			presences: []
		}
	},
	user: {
		clyde_ai: {
			id: "1081004946872352958",
			username: "clyde",
			avatar: "a_6170487d32fdfe9f988720ad80e6ab8c",
			discriminator: "0000",
			public_flags: 0,
			premium_type: 2,
			flags: 0,
			bot: true,
			banner: null,
			accent_color: null,
			global_name: "Clyde",
			avatar_decoration_data: null,
			banner_color: null
		}
	},
	member: {
		kumaccino: {
			avatar: null,
			communication_disabled_until: null,
			flags: 0,
			joined_at: "2015-11-11T09:55:40.321000+00:00",
			nick: null,
			pending: false,
			premium_since: null,
			roles: [
				"112767366235959296", "118924814567211009",
				"199995902742626304", "204427286542417920",
				"222168467627835392", "238028326281805825",
				"259806643414499328", "265239342648131584",
				"271173313575780353", "287733611912757249",
				"225744901915148298", "305775031223320577",
				"318243902521868288", "348651574924541953",
				"349185088157777920", "378402925128712193",
				"392141548932038658", "393912152173576203",
				"482860581670486028", "495384759074160642",
				"638988388740890635", "373336013109461013",
				"530220455085473813", "454567553738473472",
				"790724320824655873", "1040735082610167858",
				"695946570482450442", "849737964090556488"
			],
			user: {
				id: "113340068197859328",
				username: "kumaccino",
				avatar: "b48302623a12bc7c59a71328f72ccb39",
				discriminator: "0",
				public_flags: 128,
				flags: 128,
				banner: null,
				accent_color: 10206929,
				global_name: "kumaccino",
				avatar_decoration_data: null,
				banner_color: "#9bbed1"
			},
			mute: false,
			deaf: false
		},
		sheep: {
			avatar: "38dd359aa12bcd52dd3164126c587f8c",
			communication_disabled_until: null,
			flags: 0,
			joined_at: "2020-10-14T22:08:37.804000+00:00",
			nick: "The Expert's Submarine",
			pending: false,
			premium_since: "2022-05-04T00:28:44.326000+00:00",
			roles: [
				"112767366235959296",  "118924814567211009",
				"118923488755974146",  "199995902742626304",
				"204427286542417920",  "217013981053845504",
				"222168467627835392",  "260993819204386816",
				"265239342648131584",  "271173313575780353",
				"225744901915148298",  "287733611912757249",
				"318243902521868288",  "348651574924541953",
				"352291384021090304",  "378402925128712193",
				"392141548932038658",  "393912152173576203",
				"1123460940935991296", "872274377150980116",
				"373336013109461013",  "530220455085473813",
				"768280323829137430",  "842343433452257310",
				"454567553738473472",  "920107226528612383",
				"1123528381514911745", "1040735082610167858",
				"585531096071012409",  "849737964090556488",
				"660272211449479249"
			],
			user: {
				id: "134826546694193153",
				username: "aprilsong",
				avatar: "c754c120bce07ae3b3130e2b0e61d9dd",
				discriminator: "0",
				public_flags: 640,
				flags: 640,
				banner: "a3ad0693213f9dbf793b4159dbae0717",
				accent_color: null,
				global_name: "sheep",
				avatar_decoration: null,
				display_name: "sheep",
				banner_color: null
			},
			mute: false,
			deaf: false
		},
		papiophidian: {
			avatar: null,
			communication_disabled_until: null,
			flags: 0,
			joined_at: "2018-08-05T09:40:47.076000+00:00",
			nick: null,
			pending: false,
			premium_since: "2021-09-30T18:58:44.996000+00:00",
			roles: [
				"475599410068324352",
				"475599471049310208",
				"497586624390234112",
				"613685290938138625",
				"475603310955593729",
				"1151970058730487898",
				"1151970058730487901"
			],
			unusual_dm_activity_until: null,
			user: {
				id: "320067006521147393",
				username: "papiophidian",
				avatar: "5fc4ad85c1ea876709e9a7d3374a78a1",
				discriminator: "0",
				public_flags: 4194880,
				flags: 4194880,
				banner: "a_6f311cf6a3851a98e2fa0335af85b1d1",
				accent_color: 1579292,
				global_name: "PapiOphidian",
				avatar_decoration_data: null,
				banner_color: "#18191c"
			},
			mute: false,
			deaf: false
		}
	},
	pins: {
		faked: [
			{id: "1126786462646550579"},
			{id: "1141501302736695316"},
			{id: "1106366167788044450"},
			{id: "1115688611186193400"}
		]
	},
	message: {
		// Display order is text content, attachments, then stickers
		simple_plaintext: {
			id: "1126733830494093453",
			type: 0,
			content: "ayy lmao",
			channel_id: "112760669178241024",
			author: {
				id: "111604486476181504",
				username: "kyuugryphon",
				avatar: "e4ce31267ca524d19be80e684d4cafa1",
				discriminator: "0",
				public_flags: 0,
				flags: 0,
				banner: null,
				accent_color: null,
				global_name: "KyuuGryphon",
				avatar_decoration: null,
				display_name: "KyuuGryphon",
				banner_color: null
			},
			attachments: [],
			embeds: [],
			mentions: [],
			mention_roles: [],
			pinned: false,
			mention_everyone: false,
			tts: false,
			timestamp: "2023-07-07T04:37:58.892000+00:00",
			edited_timestamp: null,
			flags: 0,
			components: []
		},
		simple_plaintext_with_quotes: {
			id: "1126733830494093454",
			type: 0,
			content: `then he said, "you and her aren't allowed in here!"`,
			channel_id: "112760669178241024",
			author: {
				id: "111604486476181504",
				username: "kyuugryphon",
				avatar: "e4ce31267ca524d19be80e684d4cafa1",
				discriminator: "0",
				public_flags: 0,
				flags: 0,
				banner: null,
				accent_color: null,
				global_name: "KyuuGryphon",
				avatar_decoration: null,
				display_name: "KyuuGryphon",
				banner_color: null
			},
			attachments: [],
			embeds: [],
			mentions: [],
			mention_roles: [],
			pinned: false,
			mention_everyone: false,
			tts: false,
			timestamp: "2023-07-07T04:37:58.892000+00:00",
			edited_timestamp: null,
			flags: 0,
			components: []
		},
		simple_user_mention: {
			id: "1126739682080858234",
			type: 0,
			content: "<@820865262526005258> Tell me about Phil, renowned martial arts master and creator of the Chin Trick",
			channel_id: "112760669178241024",
			author: {
				id: "114147806469554185",
				username: "extremity",
				avatar: "6628aaf6b27219c36e2d3b5cfd6d0ee6",
				discriminator: "0",
				public_flags: 768,
				flags: 768,
				banner: null,
				accent_color: null,
				global_name: "Extremity",
				avatar_decoration: null,
				display_name: "Extremity",
				banner_color: null
			},
			attachments: [],
			embeds: [],
			mentions: [
				{
					id: "820865262526005258",
					username: "crunch god",
					avatar: "f7a75ca031c1d2326e0f3ca5213eea47",
					discriminator: "8889",
					public_flags: 0,
					flags: 0,
					bot: true,
					banner: null,
					accent_color: null,
					global_name: null,
					avatar_decoration: null,
					display_name: null,
					banner_color: null
				}
			],
			mention_roles: [],
			pinned: false,
			mention_everyone: false,
			tts: false,
			timestamp: "2023-07-07T05:01:14.019000+00:00",
			edited_timestamp: null,
			flags: 0,
			components: []
		},
		simple_room_mention: {
			type: 0,
			tts: false,
			timestamp: "2023-07-10T20:04:25.939000+00:00",
			referenced_message: null,
			pinned: false,
			nonce: "1128054139385806848",
			mentions: [],
			mention_roles: [],
			mention_everyone: false,
			member: {
				roles: [
					"112767366235959296",  "118924814567211009",
					"204427286542417920",  "199995902742626304",
					"222168467627835392",  "238028326281805825",
					"259806643414499328",  "265239342648131584",
					"271173313575780353",  "287733611912757249",
					"225744901915148298",  "305775031223320577",
					"318243902521868288",  "348651574924541953",
					"349185088157777920",  "378402925128712193",
					"392141548932038658",  "393912152173576203",
					"482860581670486028",  "495384759074160642",
					"638988388740890635",  "373336013109461013",
					"530220455085473813",  "454567553738473472",
					"790724320824655873",  "1123518980456452097",
					"1040735082610167858", "695946570482450442",
					"1123460940935991296", "849737964090556488"
				],
				premium_since: null,
				pending: false,
				nick: null,
				mute: false,
				joined_at: "2015-11-11T09:55:40.321000+00:00",
				flags: 0,
				deaf: false,
				communication_disabled_until: null,
				avatar: null
			},
			id: "1128054143064494233",
			flags: 0,
			embeds: [],
			edited_timestamp: null,
			content: "<#1100319550446252084>",
			components: [],
			channel_id: "266767590641238027",
			author: {
				username: "kumaccino",
				public_flags: 128,
				id: "113340068197859328",
				global_name: "kumaccino",
				discriminator: "0",
				avatar_decoration: null,
				avatar: "b48302623a12bc7c59a71328f72ccb39"
			},
			attachments: [],
			guild_id: "112760669178241024"
		},
		nicked_room_mention: {
			type: 0,
			tts: false,
			timestamp: "2023-07-10T20:04:25.939000+00:00",
			referenced_message: null,
			pinned: false,
			nonce: "1128054139385806848",
			mentions: [],
			mention_roles: [],
			mention_everyone: false,
			member: {
				roles: [
					"112767366235959296",  "118924814567211009",
					"204427286542417920",  "199995902742626304",
					"222168467627835392",  "238028326281805825",
					"259806643414499328",  "265239342648131584",
					"271173313575780353",  "287733611912757249",
					"225744901915148298",  "305775031223320577",
					"318243902521868288",  "348651574924541953",
					"349185088157777920",  "378402925128712193",
					"392141548932038658",  "393912152173576203",
					"482860581670486028",  "495384759074160642",
					"638988388740890635",  "373336013109461013",
					"530220455085473813",  "454567553738473472",
					"790724320824655873",  "1123518980456452097",
					"1040735082610167858", "695946570482450442",
					"1123460940935991296", "849737964090556488"
				],
				premium_since: null,
				pending: false,
				nick: null,
				mute: false,
				joined_at: "2015-11-11T09:55:40.321000+00:00",
				flags: 0,
				deaf: false,
				communication_disabled_until: null,
				avatar: null
			},
			id: "1128054143064494233",
			flags: 0,
			embeds: [],
			edited_timestamp: null,
			content: "<#112760669178241024>",
			components: [],
			channel_id: "266767590641238027",
			author: {
				username: "kumaccino",
				public_flags: 128,
				id: "113340068197859328",
				global_name: "kumaccino",
				discriminator: "0",
				avatar_decoration: null,
				avatar: "b48302623a12bc7c59a71328f72ccb39"
			},
			attachments: [],
			guild_id: "112760669178241024"
		},
		unknown_room_mention: {
			type: 0,
			tts: false,
			timestamp: "2023-07-10T20:04:25.939000+00:00",
			referenced_message: null,
			pinned: false,
			nonce: "1128054139385806848",
			mentions: [],
			mention_roles: [],
			mention_everyone: false,
			member: {
				roles: [],
				premium_since: null,
				pending: false,
				nick: null,
				mute: false,
				joined_at: "2015-11-11T09:55:40.321000+00:00",
				flags: 0,
				deaf: false,
				communication_disabled_until: null,
				avatar: null
			},
			id: "1128054143064494233",
			flags: 0,
			embeds: [],
			edited_timestamp: null,
			content: "<#555>",
			components: [],
			channel_id: "266767590641238027",
			author: {
				username: "kumaccino",
				public_flags: 128,
				id: "113340068197859328",
				global_name: "kumaccino",
				discriminator: "0",
				avatar_decoration: null,
				avatar: "b48302623a12bc7c59a71328f72ccb39"
			},
			attachments: [],
			guild_id: "112760669178241024"
		},
		unbridged_room_mention: {
			type: 0,
			tts: false,
			timestamp: "2023-07-10T20:04:25.939000+00:00",
			referenced_message: null,
			pinned: false,
			nonce: "1128054139385806848",
			mentions: [],
			mention_roles: [],
			mention_everyone: false,
			member: {
				roles: [],
				premium_since: null,
				pending: false,
				nick: null,
				mute: false,
				joined_at: "2015-11-11T09:55:40.321000+00:00",
				flags: 0,
				deaf: false,
				communication_disabled_until: null,
				avatar: null
			},
			id: "1128054143064494233",
			flags: 0,
			embeds: [],
			edited_timestamp: null,
			content: "<#498323546729086986>",
			components: [],
			channel_id: "266767590641238027",
			author: {
				username: "kumaccino",
				public_flags: 128,
				id: "113340068197859328",
				global_name: "kumaccino",
				discriminator: "0",
				avatar_decoration: null,
				avatar: "b48302623a12bc7c59a71328f72ccb39"
			},
			attachments: [],
			guild_id: "112760669178241024"
		},
		simple_role_mentions: {
			id: "1162374402785153106",
			type: 0,
			content: "I'm just <@&212762309364285440> testing a few role pings <@&503685967463448616> don't mind me",
			channel_id: "160197704226439168",
			author: {
				id: "772659086046658620",
				username: "cadence.worm",
				avatar: "4b5c4b28051144e4c111f0113a0f1cf1",
				discriminator: "0",
				public_flags: 0,
				flags: 0,
				banner: null,
				accent_color: null,
				global_name: "cadence",
				avatar_decoration_data: null,
				banner_color: null
			},
			attachments: [],
			embeds: [],
			mentions: [],
			mention_roles: [ "212762309364285440", "503685967463448616" ],
			pinned: false,
			mention_everyone: false,
			tts: false,
			timestamp: "2023-10-13T13:00:53.496000+00:00",
			edited_timestamp: null,
			flags: 0,
			components: []
		},
		unknown_role: {
			id: "1162374402785153106",
			type: 0,
			content: "I'm just <@&4> testing a few role pings <@&B> don't mind me",
			channel_id: "160197704226439168",
			author: {
				id: "772659086046658620",
				username: "cadence.worm",
				avatar: "4b5c4b28051144e4c111f0113a0f1cf1",
				discriminator: "0",
				public_flags: 0,
				flags: 0,
				banner: null,
				accent_color: null,
				global_name: "cadence",
				avatar_decoration_data: null,
				banner_color: null
			},
			attachments: [],
			embeds: [],
			mentions: [],
			mention_roles: [ "212762309364285440", "503685967463448616" ],
			pinned: false,
			mention_everyone: false,
			tts: false,
			timestamp: "2023-10-13T13:00:53.496000+00:00",
			edited_timestamp: null,
			flags: 0,
			components: []
		},
		simple_message_link: {
			id: "1126788210308161626",
			type: 0,
			content: "https://ptb.discord.com/channels/112760669178241024/112760669178241024/1126786462646550579",
			channel_id: "112760669178241024",
			author: {
				id: "113340068197859328",
				username: "kumaccino",
				avatar: "b48302623a12bc7c59a71328f72ccb39",
				discriminator: "0",
				public_flags: 128,
				flags: 128,
				banner: null,
				accent_color: null,
				global_name: "kumaccino",
				avatar_decoration: null,
				display_name: "kumaccino",
				banner_color: null
			},
			attachments: [],
			embeds: [],
			mentions: [],
			mention_roles: [],
			pinned: false,
			mention_everyone: false,
			tts: false,
			timestamp: "2023-07-07T08:14:04.050000+00:00",
			edited_timestamp: null,
			flags: 0,
			components: []
		},
		message_link_to_before_ooye: {
			id: "1160824382755708948",
			type: 0,
			content: "Me: I'll scroll up to find a certain message I'll send\n" +
				"_scrolls up and clicks message links for god knows how long_\n" +
				"_completely forgets what they were looking for and simply begins scrolling up to find some fun moments_\n" +
				"_stumbles upon:_ https://discord.com/channels/112760669178241024/112760669178241024/810412561941921851",
			channel_id: "112760669178241024",
			author: {
				id: "271237147401045000",
				username: "jinx",
				avatar: "a0ba563c16aff137289f67f38545807f",
				discriminator: "0",
				public_flags: 0,
				premium_type: 0,
				flags: 0,
				banner: null,
				accent_color: null,
				global_name: "Jinx",
				avatar_decoration_data: null,
				banner_color: null
			},
			attachments: [],
			embeds: [],
			mentions: [],
			mention_roles: [],
			pinned: false,
			mention_everyone: false,
			tts: false,
			timestamp: '2023-10-09T06:21:39.923000+00:00',
			edited_timestamp: null,
			flags: 0,
			components: []
		},
		message_link_from_another_server: {
			id: "1160824382755708948",
			type: 0,
			content: "Neither of these servers are known to OOYE: https://discord.com/channels/111/222/333 https://canary.discordapp.com/channels/444/555/666",
			channel_id: "112760669178241024",
			author: {
				id: "271237147401045000",
				username: "jinx",
				avatar: "a0ba563c16aff137289f67f38545807f",
				discriminator: "0",
				public_flags: 0,
				premium_type: 0,
				flags: 0,
				banner: null,
				accent_color: null,
				global_name: "Jinx",
				avatar_decoration_data: null,
				banner_color: null
			},
			attachments: [],
			embeds: [],
			mentions: [],
			mention_roles: [],
			pinned: false,
			mention_everyone: false,
			tts: false,
			timestamp: '2023-10-09T06:21:39.923000+00:00',
			edited_timestamp: null,
			flags: 0,
			components: []
		},
		simple_written_at_mention_for_matrix: {
			id: "1159030564049915915",
			type: 0,
			content: "@ash do you need anything from the store btw as I'm heading there after gym",
			channel_id: "297272183716052993",
			author: {
				id: "221902610066571260",
				username: "subtextual",
				avatar: "c108f921e2fb84981197fe2b895e6a78",
				discriminator: "0",
				public_flags: 0,
				flags: 0,
				banner: null,
				accent_color: null,
				global_name: "subtext",
				avatar_decoration_data: null,
				banner_color: null
			},
			attachments: [],
			embeds: [],
			mentions: [],
			mention_roles: [],
			pinned: false,
			mention_everyone: false,
			tts: false,
			timestamp: "2023-10-04T07:33:40.216000+00:00",
			edited_timestamp: null,
			flags: 0,
			components: []
		},
		advanced_written_at_mention_for_matrix: {
			id: "1126739682080858234",
			type: 0,
			content: "@Cadence, tell me about @Phil, the creator of the Chin Trick, who has become ever more powerful under the mentorship of @botrac4r and @huck",
			channel_id: "112760669178241024",
			author: {
				id: "114147806469554185",
				username: "extremity",
				avatar: "6628aaf6b27219c36e2d3b5cfd6d0ee6",
				discriminator: "0",
				public_flags: 768,
				flags: 768,
				banner: null,
				accent_color: null,
				global_name: "Extremity",
				avatar_decoration: null,
				display_name: "Extremity",
				banner_color: null
			},
			attachments: [],
			embeds: [],
			mentions: [],
			mention_roles: [],
			pinned: false,
			mention_everyone: false,
			tts: false,
			timestamp: "2023-07-07T05:01:14.019000+00:00",
			edited_timestamp: null,
			flags: 0,
			components: []
		},
		simple_reply: {
			id: "1126604870762369124",
			type: 19,
			content: "<redacted for privacy>",
			channel_id: "112760669178241024",
			author: {
				id: "116718249567059974",
				username: "rnl",
				avatar: "67e70f6424eead669e076b44474164c3",
				discriminator: "0",
				public_flags: 768,
				flags: 768,
				banner: null,
				accent_color: null,
				global_name: "▲",
				avatar_decoration: null,
				display_name: "▲",
				banner_color: null
			},
			attachments: [],
			embeds: [],
			mentions: [
				{
					id: "113340068197859328",
					username: "kumaccino",
					avatar: "b48302623a12bc7c59a71328f72ccb39",
					discriminator: "0",
					public_flags: 128,
					flags: 128,
					banner: null,
					accent_color: null,
					global_name: "kumaccino",
					avatar_decoration: null,
					display_name: "kumaccino",
					banner_color: null
				}
			],
			mention_roles: [],
			pinned: false,
			mention_everyone: false,
			tts: false,
			timestamp: "2023-07-06T20:05:32.496000+00:00",
			edited_timestamp: null,
			flags: 0,
			components: [],
			message_reference: {
				channel_id: "112760669178241024",
				message_id: "1126577139723026564",
				guild_id: "112760669178241024"
			},
			referenced_message: {
				id: "1126577139723026564",
				type: 0,
				content: "this message was replied to",
				channel_id: "112760669178241024",
				author: {
					id: "113340068197859328",
					username: "kumaccino",
					avatar: "b48302623a12bc7c59a71328f72ccb39",
					discriminator: "0",
					public_flags: 128,
					flags: 128,
					banner: null,
					accent_color: null,
					global_name: "kumaccino",
					avatar_decoration: null,
					display_name: "kumaccino",
					banner_color: null
				},
				attachments: [],
				embeds: [],
				mentions: [],
				mention_roles: [],
				pinned: false,
				mention_everyone: false,
				tts: false,
				timestamp: "2023-07-06T18:15:20.901000+00:00",
				edited_timestamp: null,
				flags: 0,
				components: []
			}
		},
		reply_to_unknown_message: {
			type: 19,
			content: "enigmatic",
			mentions: [
				{
					id: "1060361805152669766",
					username: "occimyy",
					avatar: "009d2bf557bca7d4f5a1d5b75a4e2eea",
					discriminator: "0",
					public_flags: 0,
					flags: 0,
					banner: null,
					accent_color: null,
					global_name: "Lily",
					avatar_decoration_data: null,
					banner_color: null,
					clan: null,
					primary_guild: null
				}
			],
			mention_roles: [],
			attachments: [],
			embeds: [],
			timestamp: "2025-02-22T23:34:14.036000+00:00",
			edited_timestamp: null,
			flags: 0,
			components: [],
			id: "1343002945670746173",
			channel_id: "392141322863116319",
			author: {
				id: "114147806469554185",
				username: "extremity",
				avatar: "0c73816563bf912ccebf1a0f1546cfe4",
				discriminator: "0",
				public_flags: 768,
				flags: 768,
				banner: null,
				accent_color: null,
				global_name: null,
				avatar_decoration_data: null,
				banner_color: null,
				clan: null,
				primary_guild: null
			},
			pinned: false,
			mention_everyone: false,
			tts: false,
			message_reference: {
				type: 0,
				channel_id: "392141322863116319",
				message_id: "1342606571380674560",
				guild_id: "112760669178241024"
			},
			position: 0,
			referenced_message: {
				type: 0,
				content: "BILLY BOB THE GREAT",
				mentions: [],
				mention_roles: [],
				attachments: [],
				embeds: [],
				timestamp: "2025-02-21T21:19:11.041000+00:00",
				edited_timestamp: null,
				flags: 0,
				components: [],
				id: "1342606571380674560",
				channel_id: "392141322863116319",
				author: {
					id: "1060361805152669766",
					username: "occimyy",
					avatar: "009d2bf557bca7d4f5a1d5b75a4e2eea",
					discriminator: "0",
					public_flags: 0,
					flags: 0,
					banner: null,
					accent_color: null,
					global_name: "Occimyy",
					avatar_decoration_data: null,
					banner_color: null,
					clan: null,
					primary_guild: null
				},
				pinned: false,
				mention_everyone: false,
				tts: false
			}
		},
		attachment_no_content: {
			id: "1124628646670389348",
			type: 0,
			content: "",
			channel_id: "497161332244742154",
			author: {
				id: "320067006521147393",
				username: "papiophidian",
				global_name: "PapiOphidian",
				avatar: "fb2b4535f7a108619e3edae12fcb16c5",
				discriminator: "0",
				public_flags: 4194880,
				avatar_decoration: null
			},
			attachments: [
				{
					id: "1124628646431297546",
					filename: "image.png",
					size: 12919,
					url: "https://cdn.discordapp.com/attachments/497161332244742154/1124628646431297546/image.png",
					proxy_url: "https://media.discordapp.net/attachments/497161332244742154/1124628646431297546/image.png",
					width: 466,
					height: 85,
					content_type: "image/png"
				}
			],
			embeds: [],
			mentions: [],
			mention_roles: [],
			pinned: false,
			mention_everyone: false,
			tts: false,
			timestamp: "2023-07-01T09:12:43.956000+00:00",
			edited_timestamp: null,
			flags: 0,
			components: []
		},
		spoiler_attachment: {
			type: 0,
			tts: false,
			timestamp: '2023-09-02T09:38:29.480000+00:00',
			referenced_message: null,
			pinned: false,
			nonce: '1147465562901708800',
			mentions: [],
			mention_roles: [],
			mention_everyone: false,
			id: '1147465564600676383',
			flags: 0,
			embeds: [],
			edited_timestamp: null,
			content: '',
			components: [],
			channel_id: '1100319550446252084',
			author: {
				username: 'cadence.worm',
				public_flags: 0,
				id: '772659086046658620',
				global_name: 'cadence',
				discriminator: '0',
				avatar_decoration_data: null,
				avatar: '4b5c4b28051144e4c111f0113a0f1cf1'
			},
			attachments: [
				{
					url: 'https://cdn.discordapp.com/attachments/1100319550446252084/1147465564307079258/SPOILER_69-GNDP-CADENCE.nfs.gci',
					size: 73792,
					proxy_url: 'https://media.discordapp.net/attachments/1100319550446252084/1147465564307079258/SPOILER_69-GNDP-CADENCE.nfs.gci',
					id: '1147465564307079258',
					flags: 8,
					filename: 'SPOILER_69-GNDP-CADENCE.nfs.gci'
				}
			],
			guild_id: '1100319549670301727'
		},
		attachment_with_description: {
			id: "1187111292243288194",
			type: 0,
			content: "",
			channel_id: "1100319550446252084",
			author: {
				id: "772659086046658620",
				username: "cadence.worm",
				avatar: "4b5c4b28051144e4c111f0113a0f1cf1",
				discriminator: "0",
				public_flags: 0,
				premium_type: 0,
				flags: 0,
				banner: null,
				accent_color: null,
				global_name: "cadence",
				avatar_decoration_data: null,
				banner_color: null
			},
			attachments: [
				{
					id: "1187111291932905554",
					filename: "image.png",
					size: 50208,
					url: "https://cdn.discordapp.com/attachments/1100319550446252084/1187111291932905554/image.png?ex=6595b28b&is=65833d8b&hm=6a3c07328749b9d0d1a612ea0cbf1711a7fe29aeaa833c12a6eb6d5db1a87ea4&",
					proxy_url: "https://media.discordapp.net/attachments/1100319550446252084/1187111291932905554/image.png?ex=6595b28b&is=65833d8b&hm=6a3c07328749b9d0d1a612ea0cbf1711a7fe29aeaa833c12a6eb6d5db1a87ea4&",
					width: 412,
					height: 228,
					description: "here is my description!",
					content_type: "image/png",
					placeholder: "C/gBBIDPqKiim3h8hpBMv8RvVw==",
					placeholder_version: 1
				}
			],
			embeds: [],
			mentions: [],
			mention_roles: [],
			pinned: false,
			mention_everyone: false,
			tts: false,
			timestamp: "2023-12-20T19:16:27.532000+00:00",
			edited_timestamp: null,
			flags: 0,
			components: []
		},
		skull_webp_attachment_with_content: {
			type: 0,
			tts: false,
			timestamp: "2023-07-10T22:06:02.805000+00:00",
			referenced_message: null,
			pinned: false,
			nonce: "1128084721398448128",
			mentions: [],
			mention_roles: [],
			mention_everyone: false,
			member: {
				roles: [
					"112767366235959296",
					"118924814567211009",
					"199995902742626304",
					"204427286542417920",
					"222168467627835392",
					"271173313575780353",
					"392141548932038658",
					"1040735082610167858",
					"372954403902193689",
					"1124134606514442300",
					"585531096071012409"
				],
				premium_since: "2022-04-20T21:11:14.016000+00:00",
				pending: false,
				nick: "Tap to add a nickname",
				mute: false,
				joined_at: "2022-04-20T20:16:02.828000+00:00",
				flags: 0,
				deaf: false,
				communication_disabled_until: null,
				avatar: "a_4ea72c7b058ad848c9d9d35479fac26e"
			},
			id: "1128084748338741392",
			flags: 0,
			embeds: [],
			edited_timestamp: null,
			content: "Image",
			components: [],
			channel_id: "112760669178241024",
			author: {
				username: "extremity",
				public_flags: 768,
				id: "114147806469554185",
				global_name: "Extremity",
				discriminator: "0",
				avatar_decoration: null,
				avatar: "6628aaf6b27219c36e2d3b5cfd6d0ee6"
			},
			attachments: [
				{
					width: 1200,
					url: "https://cdn.discordapp.com/attachments/112760669178241024/1128084747910918195/skull.webp",
					size: 74290,
					proxy_url: "https://media.discordapp.net/attachments/112760669178241024/1128084747910918195/skull.webp",
					id: "1128084747910918195",
					height: 628,
					filename: "skull.webp",
					content_type: "image/webp"
				}
			],
			guild_id: "112760669178241024"
		},
		reply_to_skull_webp_attachment_with_content: {
			type: 19,
			tts: false,
			timestamp: "2023-07-10T22:06:27.348000+00:00",
			referenced_message: {
				type: 0,
				tts: false,
				timestamp: "2023-07-10T22:06:02.805000+00:00",
				pinned: false,
				mentions: [],
				mention_roles: [],
				mention_everyone: false,
				id: "1128084748338741392",
				flags: 0,
				embeds: [],
				edited_timestamp: null,
				content: "Image",
				components: [],
				channel_id: "112760669178241024",
				author: {
					username: "extremity",
					public_flags: 768,
					id: "114147806469554185",
					global_name: "Extremity",
					discriminator: "0",
					avatar_decoration: null,
					avatar: "6628aaf6b27219c36e2d3b5cfd6d0ee6"
				},
				attachments: [
					{
						width: 1200,
						url: "https://cdn.discordapp.com/attachments/112760669178241024/1128084747910918195/skull.webp",
						size: 74290,
						proxy_url: "https://media.discordapp.net/attachments/112760669178241024/1128084747910918195/skull.webp",
						id: "1128084747910918195",
						height: 628,
						filename: "skull.webp",
						content_type: "image/webp"
					}
				]
			},
			pinned: false,
			nonce: "1128084845403045888",
			message_reference: {
				message_id: "1128084748338741392",
				guild_id: "112760669178241024",
				channel_id: "112760669178241024"
			},
			mentions: [
				{
					username: "extremity",
					public_flags: 768,
					member: {
						roles: [
							"112767366235959296",
							"118924814567211009",
							"199995902742626304",
							"204427286542417920",
							"222168467627835392",
							"271173313575780353",
							"392141548932038658",
							"1040735082610167858",
							"372954403902193689",
							"1124134606514442300",
							"585531096071012409"
						],
						premium_since: "2022-04-20T21:11:14.016000+00:00",
						pending: false,
						nick: "Tap to add a nickname",
						mute: false,
						joined_at: "2022-04-20T20:16:02.828000+00:00",
						flags: 0,
						deaf: false,
						communication_disabled_until: null,
						avatar: "a_4ea72c7b058ad848c9d9d35479fac26e"
					},
					id: "114147806469554185",
					global_name: "Extremity",
					discriminator: "0",
					avatar_decoration: null,
					avatar: "6628aaf6b27219c36e2d3b5cfd6d0ee6"
				}
			],
			mention_roles: [],
			mention_everyone: false,
			member: {
				roles: [
					"112767366235959296",
					"118924814567211009",
					"199995902742626304",
					"204427286542417920",
					"222168467627835392",
					"271173313575780353",
					"392141548932038658",
					"1040735082610167858",
					"372954403902193689",
					"1124134606514442300",
					"585531096071012409"
				],
				premium_since: "2022-04-20T21:11:14.016000+00:00",
				pending: false,
				nick: "Tap to add a nickname",
				mute: false,
				joined_at: "2022-04-20T20:16:02.828000+00:00",
				flags: 0,
				deaf: false,
				communication_disabled_until: null,
				avatar: "a_4ea72c7b058ad848c9d9d35479fac26e"
			},
			id: "1128084851279536279",
			flags: 0,
			embeds: [],
			edited_timestamp: null,
			content: "Reply",
			components: [],
			channel_id: "112760669178241024",
			author: {
				username: "extremity",
				public_flags: 768,
				id: "114147806469554185",
				global_name: "Extremity",
				discriminator: "0",
				avatar_decoration: null,
				avatar: "6628aaf6b27219c36e2d3b5cfd6d0ee6"
			},
			attachments: [
				{
					width: 2048,
					url: "https://cdn.discordapp.com/attachments/112760669178241024/1128084851023675515/RDT_20230704_0936184915846675925224905.jpg",
					size: 85906,
					proxy_url: "https://media.discordapp.net/attachments/112760669178241024/1128084851023675515/RDT_20230704_0936184915846675925224905.jpg",
					id: "1128084851023675515",
					height: 1536,
					filename: "RDT_20230704_0936184915846675925224905.jpg",
					content_type: "image/jpeg"
				}
			],
			guild_id: "112760669178241024"
		},
		simple_reply_to_matrix_user: {
			type: 19,
			tts: false,
			timestamp: "2023-07-11T00:19:04.358000+00:00",
			referenced_message: {
				webhook_id: "703458020193206272",
				type: 0,
				tts: false,
				timestamp: "2023-07-11T00:18:52.856000+00:00",
				pinned: false,
				mentions: [],
				mention_roles: [],
				mention_everyone: false,
				id: "1128118177155526666",
				flags: 0,
				embeds: [],
				edited_timestamp: null,
				content: "so can you reply to my webhook uwu",
				components: [],
				channel_id: "112760669178241024",
				author: {
					username: "cadence",
					id: "703458020193206272",
					discriminator: "0000",
					bot: true,
					avatar: "ea5413d310c85eb9edaa9db865e80155"
				},
				attachments: [],
				application_id: "684280192553844747"
			},
			pinned: false,
			nonce: "1128118222315323392",
			message_reference: {
				message_id: "1128118177155526666",
				guild_id: "112760669178241024",
				channel_id: "112760669178241024"
			},
			mentions: [],
			mention_roles: [],
			mention_everyone: false,
			member: {
				roles: [
					"112767366235959296",  "118924814567211009",
					"204427286542417920",  "199995902742626304",
					"222168467627835392",  "238028326281805825",
					"259806643414499328",  "265239342648131584",
					"271173313575780353",  "287733611912757249",
					"225744901915148298",  "305775031223320577",
					"318243902521868288",  "348651574924541953",
					"349185088157777920",  "378402925128712193",
					"392141548932038658",  "393912152173576203",
					"482860581670486028",  "495384759074160642",
					"638988388740890635",  "373336013109461013",
					"530220455085473813",  "454567553738473472",
					"790724320824655873",  "1123518980456452097",
					"1040735082610167858", "695946570482450442",
					"1123460940935991296", "849737964090556488"
				],
				premium_since: null,
				pending: false,
				nick: null,
				mute: false,
				joined_at: "2015-11-11T09:55:40.321000+00:00",
				flags: 0,
				deaf: false,
				communication_disabled_until: null,
				avatar: null
			},
			id: "1128118225398407228",
			flags: 0,
			embeds: [],
			edited_timestamp: null,
			content: "Reply",
			components: [],
			channel_id: "112760669178241024",
			author: {
				username: "kumaccino",
				public_flags: 128,
				id: "113340068197859328",
				global_name: "kumaccino",
				discriminator: "0",
				avatar_decoration: null,
				avatar: "b48302623a12bc7c59a71328f72ccb39"
			},
			attachments: [],
			guild_id: "112760669178241024"
		},
		reply_with_video: {
			id: "1197621094983676007",
			type: 19,
			content: "",
			channel_id: "112760669178241024",
			author: {
				id: "772659086046658620",
				username: "cadence.worm",
				avatar: "4b5c4b28051144e4c111f0113a0f1cf1",
				discriminator: "0",
				public_flags: 0,
				premium_type: 2,
				flags: 0,
				banner: null,
				accent_color: null,
				global_name: "cadence",
				avatar_decoration_data: null,
				banner_color: null
			},
			attachments: [
				{
					id: "1197621094786531358",
					filename: "Ins_1960637570.mp4",
					size: 860559,
					url: "https://cdn.discordapp.com/attachments/112760669178241024/1197621094786531358/Ins_1960637570.mp4?ex=65bbee8f&is=65a9798f&hm=ae14f7824c3d526c5e11c162e012e1ee405fd5776e1e9302ed80ccd86503cfda&",
					proxy_url: "https://media.discordapp.net/attachments/112760669178241024/1197621094786531358/Ins_1960637570.mp4?ex=65bbee8f&is=65a9798f&hm=ae14f7824c3d526c5e11c162e012e1ee405fd5776e1e9302ed80ccd86503cfda&",
					width: 480,
					height: 854,
					content_type: "video/mp4",
					placeholder: "wvcFBABod4gIl3enl6iqfM+s+A==",
					placeholder_version: 1
				}
			],
			embeds: [],
			mentions: [
				{
					id: "114147806469554185",
					username: "extremity",
					avatar: "e0394d500407a8fa93774e1835b8b03a",
					discriminator: "0",
					public_flags: 768,
					premium_type: 2,
					flags: 768,
					banner: null,
					accent_color: null,
					global_name: "Extremity",
					avatar_decoration_data: null,
					banner_color: null
				}
			],
			mention_roles: [],
			pinned: false,
			mention_everyone: false,
			tts: false,
			timestamp: "2024-01-18T19:18:39.768000+00:00",
			edited_timestamp: null,
			flags: 0,
			components: [],
			message_reference: {
				channel_id: "112760669178241024",
				message_id: "1197612733600895076",
				guild_id: "112760669178241024"
			},
			referenced_message: {
				id: "1197612733600895076",
				type: 0,
				content: 'deadpicord "extremity you wake up at 4am"',
				channel_id: "112760669178241024",
				author: {
					id: "114147806469554185",
					username: "extremity",
					avatar: "e0394d500407a8fa93774e1835b8b03a",
					discriminator: "0",
					public_flags: 768,
					premium_type: 2,
					flags: 768,
					banner: null,
					accent_color: null,
					global_name: "Extremity",
					avatar_decoration_data: null,
					banner_color: null
				},
				attachments: [],
				embeds: [],
				mentions: [],
				mention_roles: [],
				pinned: false,
				mention_everyone: false,
				tts: false,
				timestamp: "2024-01-18T18:45:26.259000+00:00",
				edited_timestamp: null,
				flags: 0,
				components: []
			}
		},
		voice_message: {
			id: "1112476845783388160",
			type: 0,
			content: "",
			channel_id: "1099031887500034088",
			author: {
				id: "113340068197859328",
				username: "kumaccino",
				avatar: "b48302623a12bc7c59a71328f72ccb39",
				discriminator: "0",
				public_flags: 128,
				premium_type: 0,
				flags: 128,
				banner: null,
				accent_color: null,
				global_name: "kumaccino",
				avatar_decoration_data: null,
				banner_color: null
			},
			attachments: [
				{
					id: "1112476845502365786",
					filename: "voice-message.ogg",
					size: 10584,
					url: "https://cdn.discordapp.com/attachments/1099031887500034088/1112476845502365786/voice-message.ogg?ex=65c92d4c&is=65b6b84c&hm=0654bab5027474cbe23875954fa117cf44d8914c144cd151879590fa1baf8b1c&",
					proxy_url: "https://media.discordapp.net/attachments/1099031887500034088/1112476845502365786/voice-message.ogg?ex=65c92d4c&is=65b6b84c&hm=0654bab5027474cbe23875954fa117cf44d8914c144cd151879590fa1baf8b1c&",
					duration_secs: 3.9600000381469727,
					waveform: "AAgXAAwAPBsCAAAAInEDFwAAAAAbMwATEBAAAAAAAAAAAAAAAA==",
					content_type: "audio/ogg"
				}
			],
			embeds: [],
			mentions: [],
			mention_roles: [],
			pinned: false,
			mention_everyone: false,
			tts: false,
			timestamp: "2023-05-28T20:25:48.855000+00:00",
			edited_timestamp: null,
			flags: 8192,
			components: []
		},
		misc_file: {
			id: "1174514575819931718",
			type: 0,
			content: "final final final revised draft",
			channel_id: "122155380120748034",
			author: {
				id: "142843483923677184",
				username: "huck",
				avatar: "a_1c7fda09a242d714570b4c828ef07504",
				discriminator: "0",
				public_flags: 512,
				premium_type: 2,
				flags: 512,
				banner: null,
				accent_color: null,
				global_name: null,
				avatar_decoration_data: null,
				banner_color: null
			},
			attachments: [
				{
					id: "1174514575220158545",
					filename: "the.yml",
					size: 2274,
					url: "https://cdn.discordapp.com/attachments/122155380120748034/1174514575220158545/the.yml?ex=65cd6270&is=65baed70&hm=8c5f1b571784e3c7f99628492298815884e351ae0dc7c2ae40dd22d97caf27d9&",
					proxy_url: "https://media.discordapp.net/attachments/122155380120748034/1174514575220158545/the.yml?ex=65cd6270&is=65baed70&hm=8c5f1b571784e3c7f99628492298815884e351ae0dc7c2ae40dd22d97caf27d9&",
					content_type: "text/plain; charset=utf-8",
					content_scan_version: 0
				}
			],
			embeds: [],
			mentions: [],
			mention_roles: [],
			pinned: false,
			mention_everyone: false,
			tts: false,
			timestamp: "2023-11-16T01:01:36.301000+00:00",
			edited_timestamp: null,
			flags: 0,
			components: []
		},
		simple_reply_to_reply_in_thread: {
			type: 19,
			tts: false,
			timestamp: "2023-10-12T12:35:12.721000+00:00",
			referenced_message: {
				webhook_id: "1142275246532083723",
				type: 0,
				tts: false,
				timestamp: "2023-10-12T12:35:06.578000+00:00",
				position: 1,
				pinned: false,
				mentions: [
					{
						username: "cadence.worm",
						public_flags: 0,
						id: "772659086046658620",
						global_name: "cadence",
						discriminator: "0",
						avatar_decoration_data: null,
						avatar: "4b5c4b28051144e4c111f0113a0f1cf1"
					}
				],
				mention_roles: [],
				mention_everyone: false,
				id: "1162005526675193909",
				flags: 0,
				embeds: [],
				edited_timestamp: null,
				content: "> <:L1:1144820033948762203><:L2:1144820084079087647>https://discord.com/channels/1100319549670301727/1162005314908999790/1162005501782011975 <@772659086046658620>:\n" +
				"> So what I'm wondering is about replies.\n" +
				"What about them?",
				components: [],
				channel_id: "1162005314908999790",
				author: {
					username: "cadence [they]",
					id: "1142275246532083723",
					global_name: null,
					discriminator: "0000",
					bot: true,
					avatar: "af0ead3b92cf6e448fdad80b4e7fc9e5"
				},
				attachments: [],
				application_id: "684280192553844747"
			},
			position: 2,
			pinned: false,
			nonce: "1162005551190638592",
			message_reference: {
				message_id: "1162005526675193909",
				guild_id: "1100319549670301727",
				channel_id: "1162005314908999790"
			},
			mentions: [],
			mention_roles: [],
			mention_everyone: false,
			member: {
				roles: [],
				premium_since: null,
				pending: false,
				nick: "worm",
				mute: false,
				joined_at: "2023-04-25T07:17:03.696000+00:00",
				flags: 0,
				deaf: false,
				communication_disabled_until: null,
				avatar: null
			},
			id: "1162005552440815646",
			flags: 0,
			embeds: [],
			edited_timestamp: null,
			content: "Well, they don't seem to...",
			components: [],
			channel_id: "1162005314908999790",
			author: {
				username: "cadence.worm",
				public_flags: 0,
				id: "772659086046658620",
				global_name: "cadence",
				discriminator: "0",
				avatar_decoration_data: null,
				avatar: "4b5c4b28051144e4c111f0113a0f1cf1"
			},
			attachments: [],
			guild_id: "1100319549670301727"
		},
		infinidoge_reply_to_ami_matrix_smalltext_reply_to_infinidoge: {
			type: 19,
			tts: false,
			timestamp: "2024-08-15T20:45:20.697000+00:00",
			referenced_message: {
				webhook_id: "1179987856631017473",
				type: 0,
				tts: false,
				timestamp: "2024-08-15T20:43:42.705000+00:00",
				pinned: false,
				mentions: [
					{
						username: "infinidoge1337",
						public_flags: 768,
						id: "197126718400626689",
						global_name: "Infinidoge",
						discriminator: "0",
						clan: null,
						avatar_decoration_data: null,
						avatar: "89c59de97fdb5f90999d86fed01f23fe"
					}
				],
				mention_roles: [],
				mention_everyone: false,
				id: "1273743950028607530",
				flags: 0,
				embeds: [],
				edited_timestamp: null,
				content: "> -# <:L1:1144820033948762203><:L2:1144820084079087647>https://discord.com/channels/813954820460642336/1100319550446252084/1273733346005880925 <@197126718400626689>:\n" +
				"> -# Neat that they thought of that\n" +
				'let me guess they got a lot of bug reports like "empty chest with no loot?"',
				components: [],
				channel_id: "1100319550446252084",
				author: {
					username: "Ami (she/her)",
					id: "1179987856631017473",
					global_name: null,
					discriminator: "0000",
					bot: true,
					avatar: "8b2ca50a10b39f4f908f536cd82d7953"
				},
				attachments: [],
				application_id: "684280192553844747"
			},
			pinned: false,
			nonce: "1273744360743108608",
			message_reference: {
				type: 0,
				message_id: "1273743950028607530",
				guild_id: "813954820460642336",
				channel_id: "1100319550446252084"
			},
			mentions: [],
			mention_roles: [],
			mention_everyone: false,
			member: {
				roles: [ "832496260219928577" ],
				premium_since: null,
				pending: false,
				nick: null,
				mute: false,
				joined_at: "2021-05-05T21:22:36.181000+00:00",
				flags: 0,
				deaf: false,
				communication_disabled_until: null,
				banner: null,
				avatar: null
			},
			id: "1273744361036841061",
			flags: 0,
			embeds: [],
			edited_timestamp: null,
			content: "Most likely",
			components: [],
			channel_id: "1100319550446252084",
			author: {
				username: "infinidoge1337",
				public_flags: 768,
				id: "197126718400626689",
				global_name: "Infinidoge",
				discriminator: "0",
				clan: null,
				avatar_decoration_data: null,
				avatar: "89c59de97fdb5f90999d86fed01f23fe"
			},
			attachments: [],
			guild_id: "813954820460642336"
		},
		infinidoge_reply_to_ami_matrix_smalltext_singleline_reply_to_infinidoge: {
			type: 19,
			tts: false,
			timestamp: "2024-08-15T20:45:20.697000+00:00",
			referenced_message: {
				webhook_id: "1179987856631017473",
				type: 0,
				tts: false,
				timestamp: "2024-08-15T20:43:42.705000+00:00",
				pinned: false,
				mentions: [
					{
						username: "infinidoge1337",
						public_flags: 768,
						id: "197126718400626689",
						global_name: "Infinidoge",
						discriminator: "0",
						clan: null,
						avatar_decoration_data: null,
						avatar: "89c59de97fdb5f90999d86fed01f23fe"
					}
				],
				mention_roles: [],
				mention_everyone: false,
				id: "1273743950028607530",
				flags: 0,
				embeds: [],
				edited_timestamp: null,
				content: "> -# <:L1:1144820033948762203><:L2:1144820084079087647>https://discord.com/channels/813954820460642336/1100319550446252084/1273733346005880925 <@197126718400626689>:" +
				" Neat that they thought of that\n" +
				'let me guess they got a lot of bug reports like "empty chest with no loot?"',
				components: [],
				channel_id: "1100319550446252084",
				author: {
					username: "Ami (she/her)",
					id: "1179987856631017473",
					global_name: null,
					discriminator: "0000",
					bot: true,
					avatar: "8b2ca50a10b39f4f908f536cd82d7953"
				},
				attachments: [],
				application_id: "684280192553844747"
			},
			pinned: false,
			nonce: "1273744360743108608",
			message_reference: {
				type: 0,
				message_id: "1273743950028607530",
				guild_id: "813954820460642336",
				channel_id: "1100319550446252084"
			},
			mentions: [],
			mention_roles: [],
			mention_everyone: false,
			member: {
				roles: [ "832496260219928577" ],
				premium_since: null,
				pending: false,
				nick: null,
				mute: false,
				joined_at: "2021-05-05T21:22:36.181000+00:00",
				flags: 0,
				deaf: false,
				communication_disabled_until: null,
				banner: null,
				avatar: null
			},
			id: "1273744361036841061",
			flags: 0,
			embeds: [],
			edited_timestamp: null,
			content: "Most likely",
			components: [],
			channel_id: "1100319550446252084",
			author: {
				username: "infinidoge1337",
				public_flags: 768,
				id: "197126718400626689",
				global_name: "Infinidoge",
				discriminator: "0",
				clan: null,
				avatar_decoration_data: null,
				avatar: "89c59de97fdb5f90999d86fed01f23fe"
			},
			attachments: [],
			guild_id: "813954820460642336"
		},
		sticker: {
			id: "1106366167788044450",
			type: 0,
			content: "can have attachments too",
			channel_id: "122155380120748034",
			author: {
				id: "113340068197859328",
				username: "Cookie 🍪",
				global_name: null,
				display_name: null,
				avatar: "b48302623a12bc7c59a71328f72ccb39",
				discriminator: "7766",
				public_flags: 128,
				avatar_decoration: null
			},
			attachments: [{
				id: "1106366167486038016",
				filename: "image.png",
				size: 127373,
				url: "https://cdn.discordapp.com/attachments/122155380120748034/1106366167486038016/image.png",
				proxy_url: "https://media.discordapp.net/attachments/122155380120748034/1106366167486038016/image.png",
				width: 333,
				height: 287,
				content_type: "image/png"
			}],
			embeds: [],
			mentions: [],
			mention_roles: [],
			pinned: false,
			mention_everyone: false,
			tts: false,
			timestamp: "2023-05-11T23:44:09.690000+00:00",
			edited_timestamp: null,
			flags: 0,
			components: [],
			sticker_items: [{
				id: "1106323941183717586",
				format_type: 1,
				name: "pomu puff"
			}]
		},
		lottie_sticker: {
			id: "1106366167788044450",
			type: 0,
			content: "",
			channel_id: "122155380120748034",
			author: {
				id: "113340068197859328",
				username: "Cookie 🍪",
				global_name: null,
				display_name: null,
				avatar: "b48302623a12bc7c59a71328f72ccb39",
				discriminator: "7766",
				public_flags: 128,
				avatar_decoration: null
			},
			attachments: [],
			embeds: [],
			mentions: [],
			mention_roles: [],
			pinned: false,
			mention_everyone: false,
			tts: false,
			timestamp: "2023-05-11T23:44:09.690000+00:00",
			edited_timestamp: null,
			flags: 0,
			components: [],
			sticker_items: [{
				id: "860171525772279849",
				format_type: 3,
				name: "8"
			}]
		},
		message_in_thread: {
			type: 0,
			tts: false,
			timestamp: "2023-08-19T01:55:02.063000+00:00",
			referenced_message: null,
			position: 942,
			pinned: false,
			nonce: "1142275498206822400",
			mentions: [],
			mention_roles: [],
			mention_everyone: false,
			member: {
				roles: [
					"112767366235959296",  "118924814567211009",
					"204427286542417920",  "199995902742626304",
					"222168467627835392",  "238028326281805825",
					"259806643414499328",  "265239342648131584",
					"271173313575780353",  "287733611912757249",
					"225744901915148298",  "305775031223320577",
					"318243902521868288",  "348651574924541953",
					"349185088157777920",  "378402925128712193",
					"392141548932038658",  "393912152173576203",
					"482860581670486028",  "495384759074160642",
					"638988388740890635",  "373336013109461013",
					"530220455085473813",  "454567553738473472",
					"790724320824655873",  "1123518980456452097",
					"1040735082610167858", "695946570482450442",
					"1123460940935991296", "849737964090556488"
				],
				premium_since: null,
				pending: false,
				nick: null,
				mute: false,
				joined_at: "2015-11-11T09:55:40.321000+00:00",
				flags: 0,
				deaf: false,
				communication_disabled_until: null,
				avatar: null
			},
			id: "1142275501721911467",
			flags: 0,
			embeds: [],
			edited_timestamp: null,
			content: "don't mind me, posting something for cadence",
			components: [],
			channel_id: "910283343378120754",
			author: {
				username: "kumaccino",
				public_flags: 128,
				id: "113340068197859328",
				global_name: "kumaccino",
				discriminator: "0",
				avatar_decoration_data: null,
				avatar: "b48302623a12bc7c59a71328f72ccb39"
			},
			attachments: [],
			guild_id: "112760669178241024"
		},
		single_emoji: {
			id: "1126733830494093453",
			type: 0,
			content: "<:hippo:230201364309868544>",
			channel_id: "112760669178241024",
			author: {
				id: "111604486476181504",
				username: "kyuugryphon",
				avatar: "e4ce31267ca524d19be80e684d4cafa1",
				discriminator: "0",
				public_flags: 0,
				flags: 0,
				banner: null,
				accent_color: null,
				global_name: "KyuuGryphon",
				avatar_decoration: null,
				display_name: "KyuuGryphon",
				banner_color: null
			},
			attachments: [],
			embeds: [],
			mentions: [],
			mention_roles: [],
			pinned: false,
			mention_everyone: false,
			tts: false,
			timestamp: "2023-07-07T04:37:58.892000+00:00",
			edited_timestamp: null,
			flags: 0,
			components: []
		},
		surrounded_emoji: {
			id: "1126733830494093453",
			type: 0,
			content: "h is for <:hippo:230201364309868544>!",
			channel_id: "112760669178241024",
			author: {
				id: "111604486476181504",
				username: "kyuugryphon",
				avatar: "e4ce31267ca524d19be80e684d4cafa1",
				discriminator: "0",
				public_flags: 0,
				flags: 0,
				banner: null,
				accent_color: null,
				global_name: "KyuuGryphon",
				avatar_decoration: null,
				display_name: "KyuuGryphon",
				banner_color: null
			},
			attachments: [],
			embeds: [],
			mentions: [],
			mention_roles: [],
			pinned: false,
			mention_everyone: false,
			tts: false,
			timestamp: "2023-07-07T04:37:58.892000+00:00",
			edited_timestamp: null,
			flags: 0,
			components: []
		},
		not_been_registered_emoji: {
			id: "1126733830494093453",
			type: 0,
			content: "<:Yeah:1125827250609201255>",
			channel_id: "112760669178241024",
			author: {
				id: "111604486476181504",
				username: "kyuugryphon",
				avatar: "e4ce31267ca524d19be80e684d4cafa1",
				discriminator: "0",
				public_flags: 0,
				flags: 0,
				banner: null,
				accent_color: null,
				global_name: "KyuuGryphon",
				avatar_decoration: null,
				display_name: "KyuuGryphon",
				banner_color: null
			},
			attachments: [],
			embeds: [],
			mentions: [],
			mention_roles: [],
			pinned: false,
			mention_everyone: false,
			tts: false,
			timestamp: "2023-07-07T04:37:58.892000+00:00",
			edited_timestamp: null,
			flags: 0,
			components: []
		},
		emoji_triple_long_name: {
			id: "1156394116540805170",
			type: 0,
			content: "<:brillillillilliant_move:975572106295259148><:brillillillilliant_move:975572106295259148><:brillillillilliant_move:975572106295259148>",
			channel_id: "112760669178241024",
			author: {
				id: "111604486476181504",
				username: "kyuugryphon",
				avatar: "e4ce31267ca524d19be80e684d4cafa1",
				discriminator: "0",
				public_flags: 0,
				flags: 0,
				banner: null,
				accent_color: null,
				global_name: "KyuuGryphon",
				avatar_decoration_data: null,
				banner_color: null
			},
			attachments: [],
			embeds: [],
			mentions: [],
			mention_roles: [],
			pinned: false,
			mention_everyone: false,
			tts: false,
			timestamp: "2023-09-27T00:57:22.147000+00:00",
			edited_timestamp: null,
			flags: 0,
			components: []
		},
		embed_will_be_generated_on_reply: {
			type: 19,
			content: "https://matrix.to/#/!BnKuBPCvyfOkhcUjEu:cadence.moe/$aLVZyiC3HlOu-prCSIaXlQl68I8leUdnPFiCwkgn6qM",
			mentions: [],
			mention_roles: [],
			attachments: [],
			embeds: [
				{
					type: "link",
					url: "https://matrix.to/",
					title: "Matrix - Decentralised and secure communication",
					description: "You're invited to talk on Matrix. If you don't already have a client this link will help you pick one, and join the conversation. If you already have one, this link will help you join the conversation",
					reference_id: "1278002262400176128",
					thumbnail: {
						url: "https://matrix.org/blog/img/matrix-logo.png",
						proxy_url: "https://images-ext-1.discordapp.net/external/3yPmfN-_U_7Xn8hLSG77nY9IvdtITH0GPrB6OX3JjEI/https/matrix.org/blog/img/matrix-logo.png",
						width: 800,
						height: 400,
						placeholder: "OAgOBIComJeHeId/dXgAAAAAAA==",
						placeholder_version: 1
					}
				}
			],
			timestamp: "2024-08-27T14:44:43.490000+00:00",
			edited_timestamp: null,
			flags: 0,
			components: [],
			id: "1278002262400176128",
			channel_id: "1100319550446252084",
			author: {
				id: "772659086046658620",
				username: "cadence.worm",
				avatar: "4b5c4b28051144e4c111f0113a0f1cf1",
				discriminator: "0",
				public_flags: 0,
				flags: 0,
				banner: null,
				accent_color: null,
				global_name: "cadence",
				avatar_decoration_data: null,
				banner_color: null,
				clan: null
			},
			pinned: false,
			mention_everyone: false,
			tts: false,
			message_reference: {
				type: 0,
				channel_id: "1100319550446252084",
				message_id: "1278001833876525057",
				guild_id: "1100319549670301727"
			},
			position: 0,
			referenced_message: {
				type: 0,
				content: "b",
				mentions: [],
				mention_roles: [],
				attachments: [],
				embeds: [],
				timestamp: "2024-08-27T14:43:01.322000+00:00",
				edited_timestamp: "2024-08-27T14:43:06.277000+00:00",
				flags: 0,
				components: [],
				id: "1278001833876525057",
				channel_id: "1100319550446252084",
				author: {
					id: "772659086046658620",
					username: "cadence.worm",
					avatar: "4b5c4b28051144e4c111f0113a0f1cf1",
					discriminator: "0",
					public_flags: 0,
					flags: 0,
					banner: null,
					accent_color: null,
					global_name: "cadence",
					avatar_decoration_data: null,
					banner_color: null,
					clan: null
				},
				pinned: false,
				mention_everyone: false,
				tts: false
			}
		},
		forwarded_image: {                                                                                                                                                                                                                                                                               type: 0,
			content: "",
			mentions: [],
			mention_roles: [],
			attachments: [],
			embeds: [],
			timestamp: "2024-10-16T22:25:01.973000+00:00",
			edited_timestamp: null,
			flags: 16384,
			components: [],
			id: "1296237495993892916",
			channel_id: "112760669178241024",
			author: {
				id: "113340068197859328",
				username: "kumaccino",
				avatar: "a8829abe66866d7797b36f0bfac01086",
				discriminator: "0",
				public_flags: 128,
				flags: 128,
				banner: null,
				accent_color: null,
				global_name: "kumaccino",
				avatar_decoration_data: null,
				banner_color: null,
				clan: null
			},
			pinned: false,
			mention_everyone: false,
			tts: false,
			message_reference: {
				type: 1,
				channel_id: "1019762340922663022",
				message_id: "1019779830469894234"
			},
			position: 0,
			message_snapshots: [
				{
					message: {
						type: 0,
						content: "",
						mentions: [],
						mention_roles: [],
						attachments: [
							{
								id: "1296237494987133070",
								filename: "100km.gif",
								size: 2965649,
								url: "https://cdn.discordapp.com/attachments/112760669178241024/1296237494987133070/100km.gif?ex=67118ebd&is=67103d3d&hm=8ed76d424f92f11366989f2ebc713d4f8206706ef712571e934da45b59944f77&",                                                                                  proxy_url: "https://media.discordapp.net/attachments/112760669178241024/1296237494987133070/100km.gif?ex=67118ebd&is=67103d3d&hm=8ed76d424f92f11366989f2ebc713d4f8206706ef712571e934da45b59944f77&",                                                                          width: 300,
								height: 300,
								content_type: "image/gif"
							}
						],
						embeds: [],
						timestamp: "2022-09-15T01:20:58.177000+00:00",
						edited_timestamp: null,
						flags: 0,
						components: []
					}
				}
			]
		},
		constructed_forwarded_message: {                                                                                                                                                                                                                                                                               type: 0,
			content: "",
			mentions: [],
			mention_roles: [],
			attachments: [],
			embeds: [],
			timestamp: "2024-10-16T22:25:01.973000+00:00",
			edited_timestamp: null,
			flags: 16384,
			components: [],
			id: "1296237495993892916",
			channel_id: "112760669178241024",
			author: {
				id: "113340068197859328",
				username: "kumaccino",
				avatar: "a8829abe66866d7797b36f0bfac01086",
				discriminator: "0",
				public_flags: 128,
				flags: 128,
				banner: null,
				accent_color: null,
				global_name: "kumaccino",
				avatar_decoration_data: null,
				banner_color: null,
				clan: null
			},
			pinned: false,
			mention_everyone: false,
			tts: false,
			message_reference: {
				type: 1,
				channel_id: "176333891320283136",
				message_id: "1191567971970191490"
			},
			position: 0,
			message_snapshots: [
				{
					message: {
						type: 0,
						content: "What's cooking, good looking? <:hipposcope:393635038903926784>",
						mentions: [],
						mention_roles: [],
						attachments: [
							{
								id: "1296237494987133070",
								filename: "100km.gif",
								size: 2965649,
								url: "https://cdn.discordapp.com/attachments/112760669178241024/1296237494987133070/100km.gif?ex=67118ebd&is=67103d3d&hm=8ed76d424f92f11366989f2ebc713d4f8206706ef712571e934da45b59944f77&",                                                                                  proxy_url: "https://media.discordapp.net/attachments/112760669178241024/1296237494987133070/100km.gif?ex=67118ebd&is=67103d3d&hm=8ed76d424f92f11366989f2ebc713d4f8206706ef712571e934da45b59944f77&",                                                                          width: 300,
								height: 300,
								content_type: "image/gif"
							}
						],
						embeds: [{
							type: "rich",
							title: "This man is 100 km away from your house",
							author: {
								name: "This man"
							},
							fields: [{
								name: "Distance away",
								value: "99 km"
							}, {
								name: "Distance away",
								value: "98 km"
							}]
						}],
						timestamp: "2022-09-15T01:20:58.177000+00:00",
						edited_timestamp: null,
						flags: 0,
						components: []
					}
				}
			]
		},
		constructed_forwarded_text: {                                                                                                                                                                                                                                                                               type: 0,
			content: "What's cooking everybody ‼️",
			mentions: [],
			mention_roles: [],
			attachments: [],
			embeds: [],
			timestamp: "2024-10-16T22:25:01.973000+00:00",
			edited_timestamp: null,
			flags: 16384,
			components: [],
			id: "1296237495993892916",
			channel_id: "112760669178241024",
			author: {
				id: "113340068197859328",
				username: "kumaccino",
				avatar: "a8829abe66866d7797b36f0bfac01086",
				discriminator: "0",
				public_flags: 128,
				flags: 128,
				banner: null,
				accent_color: null,
				global_name: "kumaccino",
				avatar_decoration_data: null,
				banner_color: null,
				clan: null
			},
			pinned: false,
			mention_everyone: false,
			tts: false,
			message_reference: {
				type: 1,
				channel_id: "497161350934560778",
				message_id: "0"
			},
			position: 0,
			message_snapshots: [
				{
					message: {
						type: 0,
						content: "What's cooking, good looking?",
						mentions: [],
						mention_roles: [],
						attachments: [],
						embeds: [],
						timestamp: "2022-09-15T01:20:58.177000+00:00",
						edited_timestamp: null,
						flags: 0,
						components: []
					}
				}
			]
		},
		forwarded_dont_scan_for_mentions: {
			type: 0,
			tts: false,
			timestamp: "2025-02-08T09:07:45.547000+00:00",
			position: 0,
			pinned: false,
			nonce: "1337711633497063424",
			message_snapshots: [
				{
					message: {
						type: 0,
						timestamp: "2025-02-08T09:00:07.662000+00:00",
						mentions: [],
						flags: 0,
						embeds: [],
						edited_timestamp: null,
						content: "If some folks have spare bandwidth then helping out ArchiveTeam with archiving soon to be deleted research and government data might be worthwhile https://social.luca.run/@luca/113950834185678114",
						components: [],
						attachments: []
					}
				}
			],
			message_reference: {
				type: 1,
				message_id: "1337709539516223539",
				guild_id: "500415824616620032",
				channel_id: "794935364182867968"
			},
			mentions: [],
			mention_roles: [],
			mention_everyone: false,
			member: {
				roles: [
					"1152297516755337248",
					"300045569441660938",
					"365531770420199435",
					"1035943385338482698",
					"1205645591212990515",
					"1084555882341339259"
				],
				premium_since: null,
				pending: false,
				nick: null,
				mute: false,
				joined_at: "2023-12-27T13:02:41.614000+00:00",
				flags: 0,
				deaf: false,
				communication_disabled_until: null,
				banner: null,
				avatar: null
			},
			id: "1337711460024844350",
			flags: 16384,
			embeds: [],
			edited_timestamp: null,
			content: "",
			components: [],
			channel_type: 0,
			channel_id: "286888431945252874",
			author: {
				username: "athenna2000",
				public_flags: 0,
				primary_guild: null,
				id: "620341774984151063",
				global_name: "Amelia 🍄",
				discriminator: "0",
				clan: null,
				avatar_decoration_data: null,
				avatar: "a30f5b1bf17b5a5f387f1bb49771a2f8"
			},
			attachments: [],
			guild_id: "286888431945252874"
		}
	},
	pk_message: {
		pk_reply_to_matrix: {
			id: "1202543812644306965",
			type: 0,
			content: "this is a reply",
			channel_id: "1160894080998461480",
			author: {
				id: "1195662438662680720",
				username: "special name",
				avatar: "6b44a106659e78a2550474c61889194d",
				discriminator: "0000",
				public_flags: 0,
				flags: 0,
				bot: true,
				global_name: null
			},
			attachments: [],
			embeds: [
				{
					type: "rich",
					description: "**[Reply to:](https://discord.com/channels/1160893336324931584/1160894080998461480/1202543413652881428)** now for my next experiment:",
					author: {
						name: "cadence [they] ↩️",
						icon_url: "https://cdn.discordapp.com/avatars/1162510387057545227/af0ead3b92cf6e448fdad80b4e7fc9e5.png",
						proxy_icon_url: "https://images-ext-1.discordapp.net/external/wWslraV-s-bLDwphL64YxeDm30M7PIhQQy0EQa8jpDc/https/cdn.discordapp.com/avatars/1162510387057545227/af0ead3b92cf6e448fdad80b4e7fc9e5.png"
					}
				}
			],
			mentions: [],
			mention_roles: [],
			pinned: false,
			mention_everyone: false,
			tts: false,
			timestamp: "2024-02-01T09:19:47.118000+00:00",
			edited_timestamp: null,
			flags: 0,
			components: [],
			application_id: "466378653216014359",
			webhook_id: "1195662438662680720"
		},
		pk_reply_to_discord: {
			id: "1202543812644306965",
			type: 0,
			content: "this is a reply",
			channel_id: "1160894080998461480",
			author: {
				id: "1195662438662680720",
				username: "special name",
				avatar: "6b44a106659e78a2550474c61889194d",
				discriminator: "0000",
				public_flags: 0,
				flags: 0,
				bot: true,
				global_name: null
			},
			attachments: [],
			embeds: [
				{
					type: "rich",
					description: "**[Reply to:](https://discord.com/channels/112760669178241024/112760669178241024/1141501302736695316)** some text",
					author: {
						name: "wing ↩️",
						icon_url: "https://cdn.discordapp.com/avatars/112890272819507200/47db1be7ab77e1d812a4573177af0692.png",
						proxy_icon_url: "https://images-ext-1.discordapp.net/external/wWslraV-s-bLDwphL64YxeDm30M7PIhQQy0EQa8jpDc/https/cdn.discordapp.com/avatars/112890272819507200/47db1be7ab77e1d812a4573177af0692.png"
					}
				}
			],
			mentions: [],
			mention_roles: [],
			pinned: false,
			mention_everyone: false,
			tts: false,
			timestamp: "2024-02-01T09:19:47.118000+00:00",
			edited_timestamp: null,
			flags: 0,
			components: [],
			application_id: "466378653216014359",
			webhook_id: "1195662438662680720"
		},
		pk_reply_to_matrix_attachment: {
			id: "1207486739023798332",
			type: 0,
			content: "Cat nod",
			channel_id: "1160894080998461480",
			author: {
				id: "1195662438662680720",
				username: "Azalea &flwr; 🌺",
				avatar: "48032c7bb5009701ef1aa9bd3446a67a",
				discriminator: "0000",
				public_flags: 0,
				flags: 0,
				bot: true,
				global_name: null
			},
			attachments: [],
			embeds: [
				{
					type: "rich",
					description: "*[(click to see attachment)](https://discord.com/channels/1160893336324931584/1160894080998461480/1207486471489986620)*",
					color: 14032878,
					author: {
						name: "Ampflower 🌺 ↩️",
						icon_url: "https://cdn.discordapp.com/avatars/1162510387057545227/5d8c4e541a4d8255777fe64b4caef971.png",
						proxy_icon_url: "https://images-ext-2.discordapp.net/external/RfjCRz6fSGzbIFtAqT0CGmQjJyuiD7d48mTU-CkqW5w/https/cdn.discordapp.com/avatars/1162510387057545227/5d8c4e541a4d8255777fe64b4caef971.png"
					}
				}
			],
			mentions: [],
			mention_roles: [],
			pinned: false,
			mention_everyone: false,
			tts: false,
			timestamp: "2024-02-15T00:41:12.602000+00:00",
			edited_timestamp: null,
			flags: 0,
			components: [],
			application_id: "466378653216014359",
			webhook_id: "1195662438662680720"
		}
	},
	message_mention_everyone: {
		at_everyone: {
			id: "1214510099058655252",
			type: 0,
			content: "@everyone",
			channel_id: "1100319550446252084",
			author: {
				id: "772659086046658620",
				username: "cadence.worm",
				avatar: "4b5c4b28051144e4c111f0113a0f1cf1",
				discriminator: "0",
				public_flags: 0,
				premium_type: 0,
				flags: 0,
				banner: null,
				accent_color: null,
				global_name: "cadence",
				avatar_decoration_data: null,
				banner_color: null
			},
			attachments: [],
			embeds: [],
			mentions: [],
			mention_roles: [],
			pinned: false,
			mention_everyone: true,
			tts: false,
			timestamp: "2024-03-05T09:49:32.122000+00:00",
			edited_timestamp: null,
			flags: 0,
			components: []
		},
		at_here: {
			id: "1214510192230797332",
			type: 0,
			content: "@here",
			channel_id: "1100319550446252084",
			author: {
			  id: "772659086046658620",
			  username: "cadence.worm",
			  avatar: "4b5c4b28051144e4c111f0113a0f1cf1",
			  discriminator: "0",
			  public_flags: 0,
			  premium_type: 0,
			  flags: 0,
			  banner: null,
			  accent_color: null,
			  global_name: "cadence",
			  avatar_decoration_data: null,
			  banner_color: null
			},
			attachments: [],
			embeds: [],
			mentions: [],
			mention_roles: [],
			pinned: false,
			mention_everyone: true,
			tts: false,
			timestamp: "2024-03-05T09:49:54.336000+00:00",
			edited_timestamp: null,
			flags: 0,
			components: []
		},
		at_everyone_without_permission: {
			id: "1214510346623258654",
			type: 0,
			content: "@everyone <-- this is testing that it DOESN'T mention. if this mentions everyone then my apologies.",
			channel_id: "112760669178241024",
			author: {
				id: "772659086046658620",
				username: "cadence.worm",
				avatar: "4b5c4b28051144e4c111f0113a0f1cf1",
				discriminator: "0",
				public_flags: 0,
				premium_type: 0,
				flags: 0,
				banner: null,
				accent_color: null,
				global_name: "cadence",
				avatar_decoration_data: null,
				banner_color: null
			},
			attachments: [],
			embeds: [],
			mentions: [],
			mention_roles: [],
			pinned: false,
			mention_everyone: false,
			tts: false,
			timestamp: "2024-03-05T09:50:31.146000+00:00",
			edited_timestamp: null,
			flags: 0,
			components: []
		},
		at_here_without_permission: {
			id: "1214510346623258654",
			type: 0,
			content: "@here <-- this is testing that it DOESN'T mention. if this mentions people then my apologies.",
			channel_id: "112760669178241024",
			author: {
				id: "772659086046658620",
				username: "cadence.worm",
				avatar: "4b5c4b28051144e4c111f0113a0f1cf1",
				discriminator: "0",
				public_flags: 0,
				premium_type: 0,
				flags: 0,
				banner: null,
				accent_color: null,
				global_name: "cadence",
				avatar_decoration_data: null,
				banner_color: null
			},
			attachments: [],
			embeds: [],
			mentions: [],
			mention_roles: [],
			pinned: false,
			mention_everyone: false,
			tts: false,
			timestamp: "2024-03-05T09:50:31.146000+00:00",
			edited_timestamp: null,
			flags: 0,
			components: []
		},
		at_everyone_within_link: {
			id: "1214510225885888563",
			type: 0,
			content: "https://github.com/@everyone",
			channel_id: "1100319550446252084",
			author: {
				id: "772659086046658620",
				username: "cadence.worm",
				avatar: "4b5c4b28051144e4c111f0113a0f1cf1",
				discriminator: "0",
				public_flags: 0,
				premium_type: 0,
				flags: 0,
				banner: null,
				accent_color: null,
				global_name: "cadence",
				avatar_decoration_data: null,
				banner_color: null
			},
			attachments: [],
			embeds: [],
			mentions: [],
			mention_roles: [],
			pinned: false,
			mention_everyone: false,
			tts: false,
			timestamp: "2024-03-05T09:50:02.360000+00:00",
			edited_timestamp: null,
			flags: 0,
			components: []
		}
	},
	message_with_embeds: {
		nothing_but_a_field: {
			guild_id: "497159726455455754",
			mentions: [],
			id: "1141934888862351440",
			type: 20,
			content: "",
			channel_id: "497161350934560778",
			author: {
				id: "1109360903096369153",
				username: "Amanda 🎵",
				avatar: "d56cd1b26e043ae512edae2214962faa",
				discriminator: "2192",
				public_flags: 524288,
				flags: 524288,
				bot: true,
				banner: null,
				accent_color: null,
				global_name: null,
				avatar_decoration_data: null,
				banner_color: null
			},
			attachments: [],
			embeds: [
				{
					type: "rich",
					color: 3092790,
					fields: [
						{
							name: "Amanda 🎵#2192 <:online:606664341298872324>\nwillow tree, branch 0",
							value: "**❯ Uptime:**\n3m 55s\n**❯ Memory:**\n64.45MB",
							inline: false
						}
					]
				}
			],
			mention_roles: [],
			pinned: false,
			mention_everyone: false,
			tts: false,
			timestamp: "2023-08-18T03:21:33.629000+00:00",
			edited_timestamp: null,
			flags: 0,
			components: [],
			application_id: "1109360903096369153",
			interaction: {
				id: "1141934887608254475",
				type: 2,
				name: "stats",
				user: {
					id: "320067006521147393",
					username: "papiophidian",
					avatar: "47a19b0445069b826e136da4df4259bb",
					discriminator: "0",
					public_flags: 4194880,
					flags: 4194880,
					banner: null,
					accent_color: null,
					global_name: "PapiOphidian",
					avatar_decoration_data: null,
					banner_color: null
				}
			},
			webhook_id: "1109360903096369153"
		},
		reply_with_only_embed: {
			type: 19,
			tts: false,
			timestamp: "2023-09-29T20:44:42.606000+00:00",
			referenced_message: {
				type: 19,
				tts: false,
				timestamp: "2023-09-29T20:44:42.204000+00:00",
				pinned: false,
				message_reference: {
					message_id: "1157413453921787924",
					guild_id: "1150201337112449045",
					channel_id: "1100319550446252084"
				},
				mentions: [
					{
						username: "goat_six",
						public_flags: 64,
						id: "334539029879980040",
						global_name: "GoatSixx",
						discriminator: "0",
						avatar_decoration_data: null,
						avatar: "fd87e077c6ebe4239ce573bae083ed66"
					}
				],
				mention_roles: [],
				mention_everyone: false,
				id: "1157417694728044624",
				flags: 0,
				embeds: [
					{
						url: "https://twitter.com/dynastic/status/1707484191963648161",
						type: "rich",
						timestamp: "2023-09-28T19:55:29.543000+00:00",
						reference_id: "1157417694728044624",
						footer: {
							text: "Twitter",
							proxy_icon_url: "https://images-ext-1.discordapp.net/external/bXJWV2Y_F3XSra_kEqIYXAAsI3m1meckfLhYuWzxIfI/https/abs.twimg.com/icons/apple-touch-icon-192x192.png",
							icon_url: "https://abs.twimg.com/icons/apple-touch-icon-192x192.png"
						},
						fields: [
							{ value: "119", name: "Retweets", inline: true },
							{ value: "5581", name: "Likes", inline: true }
						],
						description: "does anyone know where to find that one video of the really mysterious yam-like object being held up to a bunch of random objects, like clocks, and they have unexplained impossible reactions to it?",
						color: 1942002,
						author: {
							url: "https://twitter.com/dynastic",
							proxy_icon_url: "https://images-ext-2.discordapp.net/external/06UZNFT37nepFbzmK2FN4q-9DO_UeSaOaZQICSiMexU/https/pbs.twimg.com/profile_images/1682417899162730499/q7dQMwLq_400x400.jpg",
							name: "dynastic (@dynastic)",
							icon_url: "https://pbs.twimg.com/profile_images/1682417899162730499/q7dQMwLq_400x400.jpg"
						}
					}
				],
				edited_timestamp: null,
				content: "https://twitter.com/dynastic/status/1707484191963648161",
				components: [],
				channel_id: "1100319550446252084",
				author: {
					username: "pokemongod",
					public_flags: 0,
					id: "66255093481082800",
					global_name: "PokemonGod",
					discriminator: "0",
					avatar_decoration_data: null,
					avatar: "0cab06c4256499749cbdd4561c629f84"
				},
				attachments: []
			},
			pinned: false,
			message_reference: {
				message_id: "1157417694728044624",
				guild_id: "1150201337112449045",
				channel_id: "1100319550446252084"
			},
			mentions: [],
			mention_roles: [],
			mention_everyone: false,
			member: {
				roles: [ "1153875112832008212" ],
				premium_since: null,
				pending: false,
				nick: null,
				mute: false,
				joined_at: "2023-09-20T02:07:44.874994+00:00",
				flags: 0,
				deaf: false,
				communication_disabled_until: null,
				avatar: null
			},
			id: "1157417696414150778",
			flags: 0,
			embeds: [
				{
					url: "https://twitter.com/i/status/1707484191963648161",
					type: "rich",
					timestamp: "2023-09-28T19:55:29+00:00",
					footer: {
						text: "Twitter",
						proxy_icon_url: "https://images-ext-1.discordapp.net/external/bXJWV2Y_F3XSra_kEqIYXAAsI3m1meckfLhYuWzxIfI/https/abs.twimg.com/icons/apple-touch-icon-192x192.png",
						icon_url: "https://abs.twimg.com/icons/apple-touch-icon-192x192.png"
					},
					fields: [
						{ value: "119", name: "Retweets", inline: true },
						{ value: "5581", name: "Likes", inline: true }
					],
					description: "does anyone know where to find that one video of the really mysterious yam-like object being held up to a bunch of random objects, like clocks, and they have unexplained impossible reactions to it?",
					color: 1942002,
					author: {
						url: "https://twitter.com/i/user/719631291747078145",
						proxy_icon_url: "https://images-ext-1.discordapp.net/external/6LgXrIifZ-MhwPPiwqAomgoy93d932jZiJqLCAf79Fw/https/pbs.twimg.com/profile_images/1682417899162730499/q7dQMwLq_normal.jpg",
						name: "dynastic (@dynastic)",
						icon_url: "https://pbs.twimg.com/profile_images/1682417899162730499/q7dQMwLq_normal.jpg"
					}
				}
			],
			edited_timestamp: null,
			content: "",
			components: [],
			channel_id: "1100319550446252084",
			author: {
				username: "Twitter Video Embeds",
				public_flags: 65536,
				id: "842601826674540574",
				global_name: null,
				discriminator: "4945",
				bot: true,
				avatar_decoration_data: null,
				avatar: "6ed5bf10f953b22d47893b4655705b30"
			},
			attachments: [],
			guild_id: "1150201337112449045"
		},
		vx_image: {
			id: "1209926442981269544",
			type: 0,
			content: "https://vxtwitter.com/TomorrowCorp/status/1760330671074287875 we got a release date!!!",
			channel_id: "288058913985789953",
			author: {
				id: "113340068197859328",
				username: "kumaccino",
				avatar: "b48302623a12bc7c59a71328f72ccb39",
				discriminator: "0",
				public_flags: 128,
				premium_type: 0,
				flags: 128,
				banner: null,
				accent_color: null,
				global_name: "kumaccino",
				avatar_decoration_data: null,
				banner_color: null
			},
			attachments: [],
			embeds: [
				{
					type: "article",
					url: "https://vxtwitter.com/TomorrowCorp/status/1760330671074287875",
					title: "Tomorrow Corporation (@TomorrowCorp)",
					description: "Mark your calendar with a wet black stain! World of Goo 2 releases on May 23, 2024 on Nintendo Switch, Epic Games Store (Win/Mac), and http://WorldOfGoo2.com (Win/Mac/Linux).\n" +
						"\n" +
						"https://tomorrowcorporation.com/posts/world-of-goo-2-now-with-100-more-release-dates-and-platforms\n" +
						"\n" +
						"💖 123 🔁 36",
					color: 8388564,
					author: {
						name: "Twitter",
						url: "https://twitter.com/tomorrowcorp/status/1760330671074287875"
					},
					provider: {
						name: "vxTwitter / fixvx",
						url: "https://github.com/dylanpdx/BetterTwitFix"
					},
					thumbnail: {
						url: "https://pbs.twimg.com/media/GG3zUMGbIAAxs3h.jpg",
						proxy_url: "https://images-ext-2.discordapp.net/external/eqA-NKoXzJ0Y_l-MlwN6shFDJibC0TbPxMNWSU5IpKY/https/pbs.twimg.com/media/GG3zUMGbIAAxs3h.jpg",
						width: 1200,
						height: 1200,
						placeholder: "5SgKDwTIlqiPjIhzlspniIiNaN8It3AD",
						placeholder_version: 1
					}
				}
			],
			mentions: [],
			mention_roles: [],
			pinned: false,
			mention_everyone: false,
			tts: false,
			timestamp: "2024-02-21T18:15:43.353000+00:00",
			edited_timestamp: null,
			flags: 0,
			components: []
		},
		vx_video: {
			id: "1209804622206599190",
			type: 0,
			content: "https://vxtwitter.com/McDonalds/status/1759971752254341417",
			channel_id: "112760669178241024",
			author: {
				id: "113340068197859328",
				username: "kumaccino",
				avatar: "b48302623a12bc7c59a71328f72ccb39",
				discriminator: "0",
				public_flags: 128,
				premium_type: 0,
				flags: 128,
				banner: null,
				accent_color: null,
				global_name: "kumaccino",
				avatar_decoration_data: null,
				banner_color: null
			},
			attachments: [],
			embeds: [
				{
					type: "video",
					url: "https://vxtwitter.com/McDonalds/status/1759971752254341417",
					title: "McDonald's (@McDonalds)",
					description: "McDonald’s🤝@studiopierrot\n\n💖 89 🔁 21",
					color: 8388564,
					author: {
						name: "McDonald’s🤝@studiopierrot\n\n💖 89 🔁 21",
						url: "https://twitter.com/McDonalds/status/1759971752254341417"
					},
					provider: {
						name: "vxTwitter / fixvx",
						url: "https://github.com/dylanpdx/BetterTwitFix"
					},
					video: {
						url: "https://video.twimg.com/ext_tw_video/1759967449548541952/pu/vid/avc1/1280x720/XN1LFIJqAFBdtaoh.mp4?tag=12",
						proxy_url: "https://images-ext-1.discordapp.net/external/TInoGDskHFBRSQR0ErWEmvmzi75EO28aSyiEXs3SB8E/%3Ftag%3D12/https/video.twimg.com/ext_tw_video/1759967449548541952/pu/vid/avc1/1280x720/XN1LFIJqAFBdtaoh.mp4",
						width: 1280,
						height: 720,
						placeholder: "AggGBIAIp4iGeYdxjHgAAAAAAA==",
						placeholder_version: 1
					}
				}
			],
			mentions: [],
			mention_roles: [],
			pinned: false,
			mention_everyone: false,
			tts: false,
			timestamp: "2024-02-21T10:11:39.017000+00:00",
			edited_timestamp: null,
			flags: 0,
			components: []
		},
		youtube_video: {
			id: "1214383754479534100",
			type: 0,
			content: "https://youtu.be/kDMHHw8JqLE?si=NaqNjVTtXugHeG_E\n\n\nJutomi I'm gonna make these sounds in your walls tonight",
			channel_id: "112760669178241024",
			author: {
				id: "1060361805152669766",
				username: "occimyy",
				avatar: "3bf268de3eab1c5441da9585534d8aa5",
				discriminator: "0",
				public_flags: 0,
				premium_type: 0,
				flags: 0,
				banner: null,
				accent_color: null,
				global_name: "Occimyy",
				avatar_decoration_data: null,
				banner_color: null
			},
			attachments: [],
			embeds: [
				{
					type: "video",
					url: "https://www.youtube.com/watch?v=kDMHHw8JqLE",
					title: "Shoebill stork clattering sounds like machine guun~!! (Japan Matsue...",
					description: "twitter\n" +
						"https://twitter.com/matsuevogelpark\n" +
						"\n" +
						"The shoebill (Balaeniceps rex) also known as whalehead, whale-headed stork, or shoe-billed stork, is a very large stork-like bird. It derives its name from its enormous shoe-shaped bill\n" +
						"some people also called them the living dinosaur~~\n" +
						"\n" +
						"#shoebill #livingdinosaur #happyofunny #weirdcreature #weirdsoun...",
					color: 16711680,
					author: {
						name: "Happy O Funny",
						url: "https://www.youtube.com/channel/UCEpQ9aEb1NafpvWp5Aoizrg"
					},
					provider: { name: "YouTube", url: "https://www.youtube.com" },
					thumbnail: {
						url: "https://i.ytimg.com/vi/kDMHHw8JqLE/maxresdefault.jpg",
						proxy_url: "https://images-ext-1.discordapp.net/external/eEPOxZQXfTHqvPQJBWqsgG3wxTQN20b8LXqw3jSqyRM/https/i.ytimg.com/vi/kDMHHw8JqLE/maxresdefault.jpg",
						width: 1280,
						height: 720,
						placeholder: "WAgSDIIIdIprl4h4h4dNoEoEaQ==",
						placeholder_version: 1
					},
					video: {
						url: "https://www.youtube.com/embed/kDMHHw8JqLE",
						width: 1280,
						height: 720,
						placeholder: "WAgSDIIIdIprl4h4h4dNoEoEaQ==",
						placeholder_version: 1
					}
				}
			],
			mentions: [],
			mention_roles: [],
			pinned: false,
			mention_everyone: false,
			tts: false,
			timestamp: "2024-03-05T01:27:29.227000+00:00",
			edited_timestamp: null,
			flags: 0,
			components: []
		},
		image_embed_and_attachment: {
			id: "1157854642810654821",
			type: 0,
			content: "https://tootsuite.net/Warp-Gate2.gif\ntanget: @ monster spawner",
			channel_id: "112760669178241024",
			author: {
				id: "113340068197859328",
				username: "kumaccino",
				avatar: "b48302623a12bc7c59a71328f72ccb39",
				discriminator: "0",
				public_flags: 128,
				flags: 128,
				banner: null,
				accent_color: null,
				global_name: "kumaccino",
				avatar_decoration_data: null,
				banner_color: null
			},
			attachments: [
				{
					id: "1157854643037163610",
					filename: "Screenshot_20231001_034036.jpg",
					size: 51981,
					url: "https://cdn.discordapp.com/attachments/176333891320283136/1157854643037163610/Screenshot_20231001_034036.jpg?ex=651a1faa&is=6518ce2a&hm=eb5ca80a3fa7add8765bf404aea2028a28a2341e4a62435986bcdcf058da82f3&",
					proxy_url: "https://media.discordapp.net/attachments/176333891320283136/1157854643037163610/Screenshot_20231001_034036.jpg?ex=651a1faa&is=6518ce2a&hm=eb5ca80a3fa7add8765bf404aea2028a28a2341e4a62435986bcdcf058da82f3&",
					width: 1080,
					height: 1170,
					content_type: "image/jpeg"
				}
			],
			embeds: [
				{
					type: "image",
					url: "https://tootsuite.net/Warp-Gate2.gif",
					thumbnail: {
						url: "https://tootsuite.net/Warp-Gate2.gif",
						proxy_url: "https://images-ext-1.discordapp.net/external/Sy1ETGflxjW3iklbLgxP-Me2BXD7pMsAX2XrJ7ttaS4/https/tootsuite.net/Warp-Gate2.gif",
						width: 258,
						height: 213
					}
				}
			],
			mentions: [],
			mention_roles: [],
			pinned: false,
			mention_everyone: false,
			tts: false,
			timestamp: "2023-10-01T01:40:58.745000+00:00",
			edited_timestamp: "2023-10-01T01:42:05.631000+00:00",
			flags: 0,
			components: []
		},
		blockquote_in_embed: {
			id: "1158894131322552391",
			type: 0,
			content: "<:emoji:288858540888686602> **4 |** <#176333891320283136>",
			channel_id: "331390333810376704",
			author: {
				id: "700796664276844612",
				username: "Starboard",
				avatar: "1db8745493a3701235275be62ce05fea",
				discriminator: "9387",
				public_flags: 65536,
				flags: 65536,
				bot: true,
				banner: null,
				accent_color: null,
				global_name: null,
				avatar_decoration_data: null,
				banner_color: null
			},
			attachments: [],
			embeds: [
				{
					type: "rich",
					description: "reply draft\n" +
					"> The following is a message composed via consensus of the Stinker Council.\n" +
					"> \n" +
					"> For those who are not currently aware of our existence, we represent the organization known as Wonderland. Our previous mission centered around the assortment and study of puzzling objects, entities and other assorted phenomena. This mission was the focus of our organization for more than 28 years.\n" +
					"> \n" +
					"> Due to circumstances outside of our control, this directive has now changed. Our new mission will be the extermination of the stinker race.\n" +
					"> \n" +
					"> There will be no further communication.",
					color: 16769436,
					timestamp: "2023-10-03T19:06:01.516000+00:00",
					fields: [
						{
							name: "​",
							value: "[Go to Message](https://discord.com/channels/112760669178241024/176333891320283136/1158842413025071135)",
							inline: false
						}
					],
					author: {
						name: "minimus",
						url: "https://discord.com/channels/112760669178241024/176333891320283136/1158842413025071135",
						icon_url: "https://cdn.discordapp.com/guilds/112760669178241024/users/112760500130975744/avatars/caf8f18d190e92c280f8bc7e13f3dfb7.png",
						proxy_icon_url: "https://images-ext-2.discordapp.net/external/ufuM1hu_C6wpfbLS-RVb5iqa_X6Ht3aIj-xntAo8jjw/https/cdn.discordapp.com/guilds/112760669178241024/users/112760500130975744/avatars/caf8f18d190e92c280f8bc7e13f3dfb7.png"
					}
				}
			],
			mentions: [],
			mention_roles: [],
			pinned: false,
			mention_everyone: false,
			tts: false,
			timestamp: "2023-10-03T22:31:32.119000+00:00",
			edited_timestamp: null,
			flags: 0,
			components: []
		},
		escaping_crazy_html_tags: {
			id: "1158894131322552391",
			type: 0,
			content: "",
			channel_id: "331390333810376704",
			author: {
				id: "700796664276844612",
				username: "Starboard",
				avatar: "1db8745493a3701235275be62ce05fea",
				discriminator: "9387",
				public_flags: 65536,
				flags: 65536,
				bot: true,
				banner: null,
				accent_color: null,
				global_name: null,
				avatar_decoration_data: null,
				banner_color: null
			},
			attachments: [],
			embeds: [{
				type: "rich",
				title: "<strong>[<span data-mx-color='#123456'>Hey<script>](https://a.co/&amp;)",
				description: "<strong>[<span data-mx-color='#123456'>Hey<script>](https://a.co/&amp;)",
				url: "https://a.co/&amp;<script>",
				footer: {
					text: "<strong>[<span data-mx-color='#123456'>Hey<script>](https://a.co/&amp;)"
				},
				author: {
					name: "<strong>[<span data-mx-color='#123456'>Hey<script>](https://a.co/&amp;)",
					url: "https://a.co/&amp;<script>",
					icon_url: "https://a.co/&amp;<script>"
				},
				fields: [
					{
						name: "<strong>[<span data-mx-color='#123456'>Hey<script>](https://a.co/&amp;)",
						value: "<strong>[<span data-mx-color='#123456'>Hey<script>](https://a.co/&amp;)"
					}
				]
			}],
			mentions: [],
			mention_roles: [],
			pinned: false,
			mention_everyone: false,
			tts: false,
			timestamp: "2023-10-03T22:31:32.119000+00:00",
			edited_timestamp: null,
			flags: 0,
			components: []
		},
		title_without_url: {
			guild_id: "497159726455455754",
			mentions: [],
			id: "1141934888862351440",
			type: 20,
			content: "",
			channel_id: "497161350934560778",
			author: {
				id: "1109360903096369153",
				username: "Amanda 🎵",
				avatar: "d56cd1b26e043ae512edae2214962faa",
				discriminator: "2192",
				public_flags: 524288,
				flags: 524288,
				bot: true,
				banner: null,
				accent_color: null,
				global_name: null,
				avatar_decoration_data: null,
				banner_color: null
			},
			attachments: [],
			embeds: [
				{
					type: "rich",
					color: 3092790,
					title: "Hi, I'm Amanda!",
					description: "I condone pirating music!"
				}
			],
			mention_roles: [],
			pinned: false,
			mention_everyone: false,
			tts: false,
			timestamp: "2023-08-18T03:21:33.629000+00:00",
			edited_timestamp: null,
			flags: 0,
			components: [],
			application_id: "1109360903096369153",
			interaction: {
				id: "1141934887608254475",
				type: 2,
				name: "stats",
				user: {
					id: "320067006521147393",
					username: "papiophidian",
					avatar: "47a19b0445069b826e136da4df4259bb",
					discriminator: "0",
					public_flags: 4194880,
					flags: 4194880,
					banner: null,
					accent_color: null,
					global_name: "PapiOphidian",
					avatar_decoration_data: null,
					banner_color: null
				}
			},
			webhook_id: "1109360903096369153"
		},
		url_without_title: {
			guild_id: "497159726455455754",
			mentions: [],
			id: "1141934888862351440",
			type: 20,
			content: "",
			channel_id: "497161350934560778",
			author: {
				id: "1109360903096369153",
				username: "Amanda 🎵",
				avatar: "d56cd1b26e043ae512edae2214962faa",
				discriminator: "2192",
				public_flags: 524288,
				flags: 524288,
				bot: true,
				banner: null,
				accent_color: null,
				global_name: null,
				avatar_decoration_data: null,
				banner_color: null
			},
			attachments: [],
			embeds: [
				{
					type: "rich",
					color: 3092790,
					url: "https://amanda.moe",
					description: "I condone pirating music!"
				}
			],
			mention_roles: [],
			pinned: false,
			mention_everyone: false,
			tts: false,
			timestamp: "2023-08-18T03:21:33.629000+00:00",
			edited_timestamp: null,
			flags: 0,
			components: [],
			application_id: "1109360903096369153",
			interaction: {
				id: "1141934887608254475",
				type: 2,
				name: "stats",
				user: {
					id: "320067006521147393",
					username: "papiophidian",
					avatar: "47a19b0445069b826e136da4df4259bb",
					discriminator: "0",
					public_flags: 4194880,
					flags: 4194880,
					banner: null,
					accent_color: null,
					global_name: "PapiOphidian",
					avatar_decoration_data: null,
					banner_color: null
				}
			},
			webhook_id: "1109360903096369153"
		},
		author_without_url: {
			guild_id: "497159726455455754",
			mentions: [],
			id: "1141934888862351440",
			type: 20,
			content: "",
			channel_id: "497161350934560778",
			author: {
				id: "1109360903096369153",
				username: "Amanda 🎵",
				avatar: "d56cd1b26e043ae512edae2214962faa",
				discriminator: "2192",
				public_flags: 524288,
				flags: 524288,
				bot: true,
				banner: null,
				accent_color: null,
				global_name: null,
				avatar_decoration_data: null,
				banner_color: null
			},
			attachments: [],
			embeds: [
				{
					type: "rich",
					color: 3092790,
					author: {
						name: "Amanda"
					},
					description: "I condone pirating music!"
				}
			],
			mention_roles: [],
			pinned: false,
			mention_everyone: false,
			tts: false,
			timestamp: "2023-08-18T03:21:33.629000+00:00",
			edited_timestamp: null,
			flags: 0,
			components: [],
			application_id: "1109360903096369153",
			interaction: {
				id: "1141934887608254475",
				type: 2,
				name: "stats",
				user: {
					id: "320067006521147393",
					username: "papiophidian",
					avatar: "47a19b0445069b826e136da4df4259bb",
					discriminator: "0",
					public_flags: 4194880,
					flags: 4194880,
					banner: null,
					accent_color: null,
					global_name: "PapiOphidian",
					avatar_decoration_data: null,
					banner_color: null
				}
			},
			webhook_id: "1109360903096369153"
		},
		author_url_without_name: {
			guild_id: "497159726455455754",
			mentions: [],
			id: "1141934888862351440",
			type: 20,
			content: "",
			channel_id: "497161350934560778",
			author: {
				id: "1109360903096369153",
				username: "Amanda 🎵",
				avatar: "d56cd1b26e043ae512edae2214962faa",
				discriminator: "2192",
				public_flags: 524288,
				flags: 524288,
				bot: true,
				banner: null,
				accent_color: null,
				global_name: null,
				avatar_decoration_data: null,
				banner_color: null
			},
			attachments: [],
			embeds: [
				{
					type: "rich",
					color: 3092790,
					author: {
						url: "https://amanda.moe"
					},
					description: "I condone pirating music!"
				}
			],
			mention_roles: [],
			pinned: false,
			mention_everyone: false,
			tts: false,
			timestamp: "2023-08-18T03:21:33.629000+00:00",
			edited_timestamp: null,
			flags: 0,
			components: [],
			application_id: "1109360903096369153",
			interaction: {
				id: "1141934887608254475",
				type: 2,
				name: "stats",
				user: {
					id: "320067006521147393",
					username: "papiophidian",
					avatar: "47a19b0445069b826e136da4df4259bb",
					discriminator: "0",
					public_flags: 4194880,
					flags: 4194880,
					banner: null,
					accent_color: null,
					global_name: "PapiOphidian",
					avatar_decoration_data: null,
					banner_color: null
				}
			},
			webhook_id: "1109360903096369153"
		},
		discord_server_included_punctuation_bad_discord: {
			id: "1221672425792606349",
			type: 0,
			content: "(test https://discord.com/channels/1160894080998461480/1160894080998461480/1202543413652881428)",
			channel_id: "1160894080998461480",
			author: {
				id: "772659086046658620",
				username: "cadence.worm",
				avatar: "4b5c4b28051144e4c111f0113a0f1cf1",
				discriminator: "0",
				public_flags: 0,
				premium_type: 0,
				flags: 0,
				banner: null,
				accent_color: null,
				global_name: "cadence",
				avatar_decoration_data: null,
				banner_color: null
			},
			attachments: [],
			embeds: [
				{
					type: "article",
					url: "https://discord.com/channels/1160894080998461480/1160894080998461480/1202543413652881428)",
					title: "Discord - A New Way to Chat with Friends & Communities",
					description: "Discord is the easiest way to communicate over voice, video, and text.  Chat, hang out, and stay close with your friends and communities.",
					provider: { name: "Discord" },
					content_scan_version: 0
				}
			],
			mentions: [],
			mention_roles: [],
			pinned: false,
			mention_everyone: false,
			tts: false,
			timestamp: "2024-03-25T04:10:03.885000+00:00",
			edited_timestamp: null,
			flags: 0,
			components: []
		}
	},
	message_update: {
		edit_by_webhook: {
			application_id: "684280192553844747",
			attachments: [],
			author: {
				avatar: null,
				bot: true,
				discriminator: "0000",
				id: "700285844094845050",
				username: "cadence [they]"
			},
			channel_id: "497161350934560778",
			components: [],
			content: "test 2",
			edited_timestamp: "2023-08-17T06:29:34.167314+00:00",
			embeds: [],
			flags: 0,
			guild_id: "497159726455455754",
			id: "1141619794500649020",
			mention_everyone: false,
			mention_roles: [],
			mentions: [],
			pinned: false,
			timestamp: "2023-08-17T06:29:29.279000+00:00",
			tts: false,
			type: 0,
			webhook_id: "700285844094845050"
		},
		bot_response: {
			attachments: [],
			author: {
				avatar: "d14f47194b6ebe4da2e18a56fc6dacfd",
				avatar_decoration: null,
				bot: true,
				discriminator: "9703",
				global_name: null,
				id: "771520384671416320",
				public_flags: 0,
				username: "Bojack Horseman"
			},
			channel_id: "160197704226439168",
			components: [],
			content: "<:ae_botrac4r:551636841284108289> @cadence asked ``­``, I respond: Stop drinking paint. (No)\n\nHit <:bn_re:362741439211503616> to reroll.",
			edited_timestamp: "2023-08-16T03:06:07.128980+00:00",
			embeds: [],
			flags: 0,
			guild_id: "112760669178241024",
			id: "1141206225632112650",
			member: {
				avatar: null,
				communication_disabled_until: null,
				deaf: false,
				flags: 0,
				joined_at: "2020-10-29T23:55:31.277000+00:00",
				mute: false,
				nick: "Olmec",
				pending: false,
				premium_since: null,
				roles: [
					"112767366235959296",
					"118924814567211009",
					"392141548932038658",
					"1123460940935991296",
					"326409028601249793",
					"114526764860047367",
					"323966487763353610",
					"1107404526870335629",
					"1040735082610167858"
				]
			},
			mention_everyone: false,
			mention_roles: [],
			mentions: [
				{
					avatar: "8757ad3edee9541427edd7f817ae2f5c",
					avatar_decoration: null,
					bot: true,
					discriminator: "8559",
					global_name: null,
					id: "353703396483661824",
					member: {
						avatar: null,
						communication_disabled_until: null,
						deaf: false,
						flags: 0,
						joined_at: "2017-11-30T04:27:20.749000+00:00",
						mute: false,
						nick: null,
						pending: false,
						premium_since: null,
						roles: [
							"112767366235959296",
							"118924814567211009",
							"289671295359254529",
							"114526764860047367",
							"1040735082610167858"
						]
					},
					public_flags: 0,
					username: "botrac4r"
				}
			],
			pinned: false,
			timestamp: "2023-08-16T03:06:06.777000+00:00",
			tts: false,
			type: 0
		},
		removed_caption_from_image: {
			attachments: [
				{
					content_type: "image/png",
					filename: "piper_2.png",
					height: 163,
					id: "1141501302497615912",
					proxy_url: "https://media.discordapp.net/attachments/112760669178241024/1141501302497615912/piper_2.png",
					size: 43231,
					url: "https://cdn.discordapp.com/attachments/112760669178241024/1141501302497615912/piper_2.png",
					width: 188
				}
			],
			author: {
				avatar: "47db1be7ab77e1d812a4573177af0692",
				avatar_decoration: null,
				discriminator: "0",
				global_name: "wing",
				id: "112890272819507200",
				public_flags: 0,
				username: ".wing."
			},
			channel_id: "112760669178241024",
			components: [],
			content: "",
			edited_timestamp: "2023-08-16T22:38:43.075298+00:00",
			embeds: [],
			flags: 0,
			guild_id: "112760669178241024",
			id: "1141501302736695316",
			member: {
				avatar: null,
				communication_disabled_until: null,
				deaf: false,
				flags: 0,
				joined_at: "2015-11-08T12:25:38.461000+00:00",
				mute: false,
				nick: "windfucker",
				pending: false,
				premium_since: null,
				roles: [
					"204427286542417920",
					"118924814567211009",
					"222168467627835392",
					"265239342648131584",
					"303273332248412160",
					"303319030163439616",
					"305775031223320577",
					"318243902521868288",
					"349185088157777920",
					"378402925128712193",
					"391076926573510656",
					"230462991751970827",
					"392141548932038658",
					"397533096012152832",
					"454567553738473472",
					"482658335536185357",
					"482860581670486028",
					"495384759074160642",
					"638988388740890635",
					"764071315388629012",
					"373336013109461013",
					"872274377150980116",
					"1034022405275910164",
					"790724320824655873",
					"1040735082610167858",
					"1123730787653660742",
					"1070177137367208036"
				]
			},
			mention_everyone: false,
			mention_roles: [],
			mentions: [],
			pinned: false,
			timestamp: "2023-08-16T22:38:38.641000+00:00",
			tts: false,
			type: 0
		},
		added_caption_to_image: {
			attachments: [
				{
					content_type: "image/png",
					filename: "piper_2.png",
					height: 163,
					id: "1141501302497615912",
					proxy_url: "https://media.discordapp.net/attachments/112760669178241024/1141501302497615912/piper_2.png",
					size: 43231,
					url: "https://cdn.discordapp.com/attachments/112760669178241024/1141501302497615912/piper_2.png",
					width: 188
				}
			],
			author: {
				avatar: "47db1be7ab77e1d812a4573177af0692",
				avatar_decoration: null,
				discriminator: "0",
				global_name: "wing",
				id: "112890272819507200",
				public_flags: 0,
				username: ".wing."
			},
			channel_id: "112760669178241024",
			components: [],
			content: "some text",
			edited_timestamp: "2023-08-17T00:13:18.620975+00:00",
			embeds: [],
			flags: 0,
			guild_id: "112760669178241024",
			id: "1141501302736695317",
			member: {
				avatar: null,
				communication_disabled_until: null,
				deaf: false,
				flags: 0,
				joined_at: "2015-11-08T12:25:38.461000+00:00",
				mute: false,
				nick: "windfucker",
				pending: false,
				premium_since: null,
				roles: [
					"204427286542417920",
					"118924814567211009",
					"222168467627835392",
					"265239342648131584",
					"303273332248412160",
					"303319030163439616",
					"305775031223320577",
					"318243902521868288",
					"349185088157777920",
					"378402925128712193",
					"391076926573510656",
					"230462991751970827",
					"392141548932038658",
					"397533096012152832",
					"454567553738473472",
					"482658335536185357",
					"482860581670486028",
					"495384759074160642",
					"638988388740890635",
					"764071315388629012",
					"373336013109461013",
					"872274377150980116",
					"1034022405275910164",
					"790724320824655873",
					"1040735082610167858",
					"1123730787653660742",
					"1070177137367208036"
				]
			},
			mention_everyone: false,
			mention_roles: [],
			mentions: [],
			pinned: false,
			timestamp: "2023-08-16T22:38:38.641000+00:00",
			tts: false,
			type: 0
		},
		changed_file_type: {
			attachments: [
				{
					content_type: "text/plain",
					filename: "gaze_into_my_dark_mind.txt",
					size: 20102199,
					url: "https://cdn.discordapp.com/attachments/112760669178241024/1141501302497615912/gaze_into_my_dark_mind.txt",
				}
			],
			author: {
				avatar: "47db1be7ab77e1d812a4573177af0692",
				avatar_decoration: null,
				discriminator: "0",
				global_name: "wing",
				id: "112890272819507200",
				public_flags: 0,
				username: ".wing."
			},
			channel_id: "112760669178241024",
			components: [],
			content: "",
			edited_timestamp: "2023-08-17T00:13:18.620975+00:00",
			embeds: [],
			flags: 0,
			guild_id: "112760669178241024",
			id: "1141501302736695317",
			member: {
				avatar: null,
				communication_disabled_until: null,
				deaf: false,
				flags: 0,
				joined_at: "2015-11-08T12:25:38.461000+00:00",
				mute: false,
				nick: "windfucker",
				pending: false,
				premium_since: null,
				roles: [
					"204427286542417920",
					"118924814567211009",
					"222168467627835392",
					"265239342648131584",
					"303273332248412160",
					"303319030163439616",
					"305775031223320577",
					"318243902521868288",
					"349185088157777920",
					"378402925128712193",
					"391076926573510656",
					"230462991751970827",
					"392141548932038658",
					"397533096012152832",
					"454567553738473472",
					"482658335536185357",
					"482860581670486028",
					"495384759074160642",
					"638988388740890635",
					"764071315388629012",
					"373336013109461013",
					"872274377150980116",
					"1034022405275910164",
					"790724320824655873",
					"1040735082610167858",
					"1123730787653660742",
					"1070177137367208036"
				]
			},
			mention_everyone: false,
			mention_roles: [],
			mentions: [],
			pinned: false,
			timestamp: "2023-08-16T22:38:38.641000+00:00",
			tts: false,
			type: 0
		},
		edited_content_with_sticker_and_attachments: {
			id: "1106366167788044450",
			type: 0,
			content: "only the content can be edited",
			channel_id: "122155380120748034",
			author: {
				id: "113340068197859328",
				username: "Cookie 🍪",
				global_name: null,
				display_name: null,
				avatar: "b48302623a12bc7c59a71328f72ccb39",
				discriminator: "7766",
				public_flags: 128,
				avatar_decoration: null
			},
			attachments: [{
				id: "1106366167486038016",
				filename: "image.png",
				size: 127373,
				url: "https://cdn.discordapp.com/attachments/122155380120748034/1106366167486038016/image.png",
				proxy_url: "https://media.discordapp.net/attachments/122155380120748034/1106366167486038016/image.png",
				width: 333,
				height: 287,
				content_type: "image/png"
			}],
			embeds: [],
			mentions: [],
			mention_roles: [],
			pinned: false,
			mention_everyone: false,
			tts: false,
			timestamp: "2023-05-11T23:44:09.690000+00:00",
			edited_timestamp: "2023-05-11T23:44:19.690000+00:00",
			flags: 0,
			components: [],
			sticker_items: [{
				id: "1106323941183717586",
				format_type: 1,
				name: "pomu puff"
			}]
		},
		edited_content_with_sticker_and_attachments_but_all_parts_equal_0: {
			id: "1106366167788044451",
			type: 0,
			content: "only the content can be edited",
			channel_id: "122155380120748034",
			author: {
				id: "113340068197859328",
				username: "Cookie 🍪",
				global_name: null,
				display_name: null,
				avatar: "b48302623a12bc7c59a71328f72ccb39",
				discriminator: "7766",
				public_flags: 128,
				avatar_decoration: null
			},
			attachments: [{
				id: "1106366167486038016",
				filename: "image.png",
				size: 127373,
				url: "https://cdn.discordapp.com/attachments/122155380120748034/1106366167486038016/image.png",
				proxy_url: "https://media.discordapp.net/attachments/122155380120748034/1106366167486038016/image.png",
				width: 333,
				height: 287,
				content_type: "image/png"
			}],
			embeds: [],
			mentions: [],
			mention_roles: [],
			pinned: false,
			mention_everyone: false,
			tts: false,
			timestamp: "2023-05-11T23:44:09.690000+00:00",
			edited_timestamp: "2023-05-11T23:44:19.690000+00:00",
			flags: 0,
			components: [],
			sticker_items: [{
				id: "1106323941183717586",
				format_type: 1,
				name: "pomu puff"
			}]
		},
		edited_content_with_sticker_and_attachments_but_all_parts_equal_1: {
			id: "1106366167788044452",
			type: 0,
			content: "only the content can be edited",
			channel_id: "122155380120748034",
			author: {
				id: "113340068197859328",
				username: "Cookie 🍪",
				global_name: null,
				display_name: null,
				avatar: "b48302623a12bc7c59a71328f72ccb39",
				discriminator: "7766",
				public_flags: 128,
				avatar_decoration: null
			},
			attachments: [{
				id: "1106366167486038016",
				filename: "image.png",
				size: 127373,
				url: "https://cdn.discordapp.com/attachments/122155380120748034/1106366167486038016/image.png",
				proxy_url: "https://media.discordapp.net/attachments/122155380120748034/1106366167486038016/image.png",
				width: 333,
				height: 287,
				content_type: "image/png"
			}],
			embeds: [],
			mentions: [],
			mention_roles: [],
			pinned: false,
			mention_everyone: false,
			tts: false,
			timestamp: "2023-05-11T23:44:09.690000+00:00",
			edited_timestamp: "2023-05-11T23:44:19.690000+00:00",
			flags: 0,
			components: [],
			sticker_items: [{
				id: "1106323941183717586",
				format_type: 1,
				name: "pomu puff"
			}]
		},
		edit_of_reply_to_skull_webp_attachment_with_content: {
			type: 19,
			tts: false,
			timestamp: "2023-07-10T22:06:27.348000+00:00",
			referenced_message: {
				type: 0,
				tts: false,
				timestamp: "2023-07-10T22:06:02.805000+00:00",
				pinned: false,
				mentions: [],
				mention_roles: [],
				mention_everyone: false,
				id: "1128084748338741392",
				flags: 0,
				embeds: [],
				edited_timestamp: null,
				content: "Image",
				components: [],
				channel_id: "112760669178241024",
				author: {
					username: "extremity",
					public_flags: 768,
					id: "114147806469554185",
					global_name: "Extremity",
					discriminator: "0",
					avatar_decoration: null,
					avatar: "6628aaf6b27219c36e2d3b5cfd6d0ee6"
				},
				attachments: [
					{
						width: 1200,
						url: "https://cdn.discordapp.com/attachments/112760669178241024/1128084747910918195/skull.webp",
						size: 74290,
						proxy_url: "https://media.discordapp.net/attachments/112760669178241024/1128084747910918195/skull.webp",
						id: "1128084747910918195",
						height: 628,
						filename: "skull.webp",
						content_type: "image/webp"
					}
				]
			},
			pinned: false,
			message_reference: {
				message_id: "1128084748338741392",
				guild_id: "112760669178241024",
				channel_id: "112760669178241024"
			},
			mentions: [
				{
					username: "extremity",
					public_flags: 768,
					member: {
						roles: [
							"112767366235959296",
							"118924814567211009",
							"199995902742626304",
							"204427286542417920",
							"222168467627835392",
							"271173313575780353",
							"392141548932038658",
							"1040735082610167858",
							"372954403902193689",
							"1124134606514442300",
							"585531096071012409"
						],
						premium_since: "2022-04-20T21:11:14.016000+00:00",
						pending: false,
						nick: "Tap to add a nickname",
						mute: false,
						joined_at: "2022-04-20T20:16:02.828000+00:00",
						flags: 0,
						deaf: false,
						communication_disabled_until: null,
						avatar: "a_4ea72c7b058ad848c9d9d35479fac26e"
					},
					id: "114147806469554185",
					global_name: "Extremity",
					discriminator: "0",
					avatar_decoration: null,
					avatar: "6628aaf6b27219c36e2d3b5cfd6d0ee6"
				}
			],
			mention_roles: [],
			mention_everyone: false,
			member: {
				roles: [
					"112767366235959296",
					"118924814567211009",
					"199995902742626304",
					"204427286542417920",
					"222168467627835392",
					"271173313575780353",
					"392141548932038658",
					"1040735082610167858",
					"372954403902193689",
					"1124134606514442300",
					"585531096071012409"
				],
				premium_since: "2022-04-20T21:11:14.016000+00:00",
				pending: false,
				nick: "Tap to add a nickname",
				mute: false,
				joined_at: "2022-04-20T20:16:02.828000+00:00",
				flags: 0,
				deaf: false,
				communication_disabled_until: null,
				avatar: "a_4ea72c7b058ad848c9d9d35479fac26e"
			},
			id: "1128084851279536279",
			flags: 0,
			embeds: [],
			edited_timestamp: "2023-07-10T22:08:57.442417+00:00",
			content: "Edit",
			components: [],
			channel_id: "112760669178241024",
			author: {
				username: "extremity",
				public_flags: 768,
				id: "114147806469554185",
				global_name: "Extremity",
				discriminator: "0",
				avatar_decoration: null,
				avatar: "6628aaf6b27219c36e2d3b5cfd6d0ee6"
			},
			attachments: [
				{
					width: 2048,
					url: "https://cdn.discordapp.com/attachments/112760669178241024/1128084851023675515/RDT_20230704_0936184915846675925224905.jpg",
					size: 85906,
					proxy_url: "https://media.discordapp.net/attachments/112760669178241024/1128084851023675515/RDT_20230704_0936184915846675925224905.jpg",
					id: "1128084851023675515",
					height: 1536,
					filename: "RDT_20230704_0936184915846675925224905.jpg",
					content_type: "image/jpeg"
				}
			],
			guild_id: "112760669178241024"
		},
		embed_generated_social_media_image: {
			channel_id: "112760669178241024",
			embeds: [
				{
					color: 8594767,
					description: "1v1 physical card game. Each player gets one standard deck of cards with a different backing to differentiate. Every turn proceeds as follows:\n\n * Both players draw eight cards\n * Both players may choose up to eight cards to discard, then draw that number of cards to put back in their hand\n * Both players present their best five-or-less-card pok...",
					provider: {
						name: "hthrflwrs on cohost"
					},
					thumbnail: {
						height: 1587,
						placeholder: "GpoKP5BJZphshnhwmmmYlmh3l7+m+mwJ",
						placeholder_version: 1,
						proxy_url: "https://images-ext-2.discordapp.net/external/9vTXIzlXU4wyUZvWfmlmQkck8nGLUL-A090W4lWsZ48/https/staging.cohostcdn.org/avatar/292-6b64b03c-4ada-42f6-8452-109275bfe68d-profile.png",
						url: "https://staging.cohostcdn.org/avatar/292-6b64b03c-4ada-42f6-8452-109275bfe68d-profile.png",
						width: 1644
					},
					title: "This post nerdsniped me, so here's some RULES FOR REAL-LIFE BALATRO",
					type: "link",
					url: "https://cohost.org/jkap/post/4794219-empty"
				}
			],
			guild_id: "112760669178241024",
			id: "1210387798297682020"
		},
		embed_generated_on_reply: {
			attachments: [],
			author: {
				avatar: "4b5c4b28051144e4c111f0113a0f1cf1",
				avatar_decoration_data: null,
				clan: null,
				discriminator: "0",
				global_name: "cadence",
				id: "772659086046658620",
				public_flags: 0,
				username: "cadence.worm"
			},
			channel_id: "1100319550446252084",
			components: [],
			content: "https://matrix.to/#/!BnKuBPCvyfOkhcUjEu:cadence.moe/$aLVZyiC3HlOu-prCSIaXlQl68I8leUdnPFiCwkgn6qM",
			edited_timestamp: null,
			embeds: [
				{
					description: "You're invited to talk on Matrix. If you don't already have a client this link will help you pick one, and join the conversation. If you already have one, this link will help you join the conversation",
					reference_id: "1278002262400176128",
					thumbnail: {
						height: 400,
						placeholder: "OAgOBIComJeHeId/dXgAAAAAAA==",
						placeholder_version: 1,
						proxy_url: "https://images-ext-1.discordapp.net/external/3yPmfN-_U_7Xn8hLSG77nY9IvdtITH0GPrB6OX3JjEI/https/matrix.org/blog/img/matrix-logo.png",
						url: "https://matrix.org/blog/img/matrix-logo.png",
						width: 800
					},
					title: "Matrix - Decentralised and secure communication",
					type: "link",
					url: "https://matrix.to/"
				}
			],
			flags: 0,
			guild_id: "1100319549670301727",
			id: "1278002262400176128",
			member: {
				avatar: null,
				banner: null,
				communication_disabled_until: null,
				deaf: false,
				flags: 0,
				joined_at: "2023-04-25T07:17:03.696000+00:00",
				mute: false,
				nick: "worm",
				pending: false,
				premium_since: null,
				roles: []
			},
			mention_everyone: false,
			mention_roles: [],
			mentions: [],
			message_reference: {
				channel_id: "1100319550446252084",
				guild_id: "1100319549670301727",
				message_id: "1278001833876525057",
				type: 0
			},
			pinned: false,
			position: 0,
			timestamp: "2024-08-27T14:44:43.490000+00:00",
			tts: false,
			type: 19
		}
	},
	special_message: {
		thread_name_change: {
			id: "1142391602799710298",
			type: 4,
			content: "worming",
			channel_id: "1142271000067706880",
			author: {
				id: "772659086046658620",
				username: "cadence.worm",
				avatar: "4b5c4b28051144e4c111f0113a0f1cf1",
				discriminator: "0",
				public_flags: 0,
				flags: 0,
				banner: null,
				accent_color: null,
				global_name: "cadence",
				avatar_decoration_data: null,
				banner_color: null
			},
			attachments: [],
			embeds: [],
			mentions: [],
			mention_roles: [],
			pinned: false,
			mention_everyone: false,
			tts: false,
			timestamp: "2023-08-19T09:36:22.717000+00:00",
			edited_timestamp: null,
			flags: 0,
			components: [],
			position: 12
		},
		updated_to_start_thread_from_here: {
			t: "MESSAGE_UPDATE",
			s: 19,
			op: 0,
			d: {
				id: "1143121514925928541",
				flags: 32,
				channel_id: "1100319550446252084",
				guild_id: "1100319549670301727"
			},
			shard_id: 0
		},
		thread_start_context: {
			type: 21,
			tts: false,
			timestamp: "2023-08-21T09:57:12.558000+00:00",
			position: 0,
			pinned: false,
			message_reference: {
				message_id: "1143121514925928541",
				guild_id: "1100319549670301727",
				channel_id: "1100319550446252084"
			},
			mentions: [],
			mention_roles: [],
			mention_everyone: false,
			member: {
				roles: [],
				premium_since: null,
				pending: false,
				nick: "worm",
				mute: false,
				joined_at: "2023-04-25T07:17:03.696000+00:00",
				flags: 0,
				deaf: false,
				communication_disabled_until: null,
				avatar: null
			},
			id: "1143121620744032327",
			flags: 0,
			embeds: [],
			edited_timestamp: null,
			content: "",
			components: [],
			channel_id: "1143121514925928541",
			author: {
				username: "cadence.worm",
				public_flags: 0,
				id: "772659086046658620",
				global_name: "cadence",
				discriminator: "0",
				avatar_decoration_data: null,
				avatar: "4b5c4b28051144e4c111f0113a0f1cf1"
			},
			attachments: [],
			guild_id: "1100319549670301727"
		},
		bridge_echo_webhook: {
			webhook_id: "1160692755144654970",
			type: 0,
			tts: false,
			timestamp: "2023-10-09T21:15:58.866000+00:00",
			pinned: false,
			mentions: [],
			mention_roles: [],
			mention_everyone: false,
			id: "1161049444674973706",
			flags: 0,
			embeds: [],
			edited_timestamp: null,
			content: "ready when you are",
			components: [],
			channel_id: "497161350934560778",
			author: {
				username: "cadence [they]",
				id: "1160692755144654970",
				discriminator: "0000",
				bot: true,
				avatar: "af0ead3b92cf6e448fdad80b4e7fc9e5"
			},
			attachments: [],
			application_id: "684280192553844747",
			guild_id: "497159726455455754"
		},
		crosspost_announcement: {
			id: "1152745817678028840",
			type: 0,
			content: "All text based commands are now inactive on Chewey Bot\nTo continue using commands you'll need to use them as slash commands",
			channel_id: "500454381414514688",
			author: {
				id: "748007224353226832",
				username: "Chewey Bot Official Server #announcements",
				avatar: "427b2893c574b90f1c6bb54da2c609cb",
				discriminator: "0000",
				public_flags: 0,
				flags: 0,
				bot: true
			},
			attachments: [],
			embeds: [],
			mentions: [],
			mention_roles: [],
			pinned: false,
			mention_everyone: false,
			tts: false,
			timestamp: "2023-09-16T23:20:19.916000+00:00",
			edited_timestamp: null,
			flags: 2,
			components: [],
			webhook_id: "748007224353226832",
			message_reference: {
				channel_id: "372274661439832065",
				message_id: "1152745799596384263",
				guild_id: "372271956562542592"
			}
		},
		reply_with_missing_referenced_message: {
			type: 19,
			content: "there must have been 2 different thumbnails used - the other one was this I think: https://lostmediawiki.com/w/images/6/6d/YOUTUBE_HACKED%21.jpg",
			mentions: [
				{
					id: "112816036671184896",
					username: "accavish",
					avatar: "0efadd2b09568c89e81f47d321c1db9f",
					discriminator: "0",
					public_flags: 0,
					flags: 0,
					banner: null,
					accent_color: null,
					global_name: "Yuv.yuv",
					avatar_decoration_data: null,
					banner_color: null,
					clan: null
				}
			],
			mention_roles: [],
			attachments: [],
			embeds: [
				{
					type: "image",
					url: "https://lostmediawiki.com/w/images/6/6d/YOUTUBE_HACKED%21.jpg",
					reference_id: "1277081824962809919",
					thumbnail: {
						url: "https://lostmediawiki.com/w/images/6/6d/YOUTUBE_HACKED%21.jpg",
						proxy_url: "https://images-ext-1.discordapp.net/external/DqHSi6Hsvkn8CeYqcpNsgcPun_yykRNdKzyPTLxkrJ8/https/lostmediawiki.com/w/images/6/6d/YOUTUBE_HACKED%2521.jpg",
						width: 898,
						height: 459,
						placeholder: "MPcFLIoEgndse3pydlUHeZSQZw==",
						placeholder_version: 1
					}
				}
			],
			timestamp: "2024-08-25T01:47:14.104000+00:00",
			edited_timestamp: null,
			flags: 0,
			components: [],
			id: "1277081824962809919",
			channel_id: "112760669178241024",
			author: {
				id: "628334893109215263",
				username: "thecracksoverhead",
				avatar: "e4eaad082f5ff0359cafa8d3ad5ddd4f",
				discriminator: "0",
				public_flags: 0,
				flags: 0,
				banner: null,
				accent_color: null,
				global_name: "jdl",
				avatar_decoration_data: null,
				banner_color: null,
				clan: null
			},
			pinned: false,
			mention_everyone: false,
			tts: false,
			message_reference: {
				type: 0,
				channel_id: "112760669178241024",
				message_id: "1277081326008143934",
				guild_id: "112760669178241024"
			},
			position: 0
		},
		ephemeral_message: {
			webhook_id: "684280192553844747",
			type: 20,
			tts: false,
			timestamp: "2024-09-29T11:22:04.865000+00:00",
			position: 0,
			pinned: false,
			nonce: "1289910062243905536",
			mentions: [],
			mention_roles: [],
			mention_everyone: false,
			interaction_metadata: {
				user: {baby: true},
				type: 2,
				name: "invite",
				id: "1289910063691206717",
				command_type: 1,
				authorizing_integration_owners: {baby: true}
			},
			interaction: {
				user: {baby: true},
				type: 2,
				name: "invite",
				id: "1289910063691206717"
			},
			id: "1289910064995504182",
			flags: 64,
			embeds: [],
			edited_timestamp: null,
			content: "`@cadence:cadence.moe` is already in this server and this channel.",
			components: [],
			channel_id: "1100319550446252084",
			author: {
				username: "Matrix Bridge",
				public_flags: 0,
				id: "684280192553844747",
				global_name: null,
				discriminator: "5728",
				clan: null,
				bot: true,
				avatar_decoration_data: null,
				avatar: "48ae3c24f2a6ec5c60c41bdabd904018"
			},
			attachments: [],
			application_id: "684280192553844747"
		},
		shard_id: 0
	},
	interaction_message: {
		thinking_interaction_without_bot_user: {
			webhook_id: "1109360903096369153",
			type: 20,
			tts: false,
			timestamp: "2023-10-09T21:16:11.673000+00:00",
			pinned: false,
			nonce: "1161049469261709312",
			mentions: [],
			mention_roles: [],
			mention_everyone: false,
			interaction: {
				user: {
					username: "papiophidian",
					public_flags: 4194880,
					id: "320067006521147393",
					global_name: "PapiOphidian",
					discriminator: "0",
					avatar_decoration_data: null,
					avatar: "5fc4ad85c1ea876709e9a7d3374a78a1"
				},
				type: 2,
				name: "stats",
				member: {
					roles: [],
					premium_since: null,
					pending: false,
					nick: "Brad",
					mute: false,
					joined_at: "2018-10-03T21:35:50.974000+00:00",
					flags: 0,
					deaf: false,
					communication_disabled_until: null,
					avatar: null
				},
				id: "1161049497724534825"
			},
			id: "1161049498391425196",
			flags: 128,
			embeds: [],
			edited_timestamp: null,
			content: "",
			components: [],
			channel_id: "497161350934560778",
			author: {
				username: "Amanda 🎵",
				public_flags: 524288,
				id: "1109360903096369153",
				global_name: null,
				discriminator: "2192",
				bot: true,
				avatar_decoration_data: null,
				avatar: "e4a45abe5f8ee44f0b59b79a08bdb2ac"
			},
			attachments: [],
			application_id: "1109360903096369153",
			guild_id: "497159726455455754"
		},
		thinking_interaction: {
			webhook_id: "1109360903096369153",
			type: 20,
			tts: false,
			timestamp: "2023-10-09T21:18:45.002000+00:00",
			pinned: false,
			nonce: "1161050112089128960",
			mentions: [],
			mention_roles: [],
			mention_everyone: false,
			member: {
				roles: [ "604073998749270046" ],
				premium_since: null,
				pending: false,
				nick: null,
				mute: false,
				joined_at: "2023-10-09T21:18:30.600000+00:00",
				flags: 1,
				deaf: false,
				communication_disabled_until: null,
				avatar: null
			},
			interaction: {
				user: {
					username: "papiophidian",
					public_flags: 4194880,
					id: "320067006521147393",
					global_name: "PapiOphidian",
					discriminator: "0",
					avatar_decoration_data: null,
					avatar: "5fc4ad85c1ea876709e9a7d3374a78a1"
				},
				type: 2,
				name: "stats",
				member: {
					roles: [],
					premium_since: null,
					pending: false,
					nick: "Brad",
					mute: false,
					joined_at: "2018-10-03T21:35:50.974000+00:00",
					flags: 0,
					deaf: false,
					communication_disabled_until: null,
					avatar: null
				},
				id: "1161050140640018472"
			},
			id: "1161050141499863120",
			flags: 128,
			embeds: [],
			edited_timestamp: null,
			content: "",
			components: [],
			channel_id: "497161350934560778",
			author: {
				username: "Amanda 🎵",
				public_flags: 524288,
				id: "1109360903096369153",
				global_name: null,
				discriminator: "2192",
				bot: true,
				avatar_decoration_data: null,
				avatar: "e4a45abe5f8ee44f0b59b79a08bdb2ac"
			},
			attachments: [],
			application_id: "1109360903096369153",
			guild_id: "497159726455455754"
		}
	}
}
