BEGIN TRANSACTION;

ALTER TABLE guild_space ADD COLUMN presence INTEGER NOT NULL DEFAULT 1;

COMMIT;
